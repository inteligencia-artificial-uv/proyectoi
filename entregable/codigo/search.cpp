/*
  *Nombre archivo: scene.cpp
  *Autor: María Andrea Cruz Blandón
  *       Yerminson Doney Gonzalez Muñoz
  *       Cristian Leonardo Ríos López
  *Fecha de creación: Septiembre 2011
  *Fecha de ultima modificación: Diciembre 2011
  *Universidad del Valle
*/

#include "search.h"
#include "comparisonuniformcost.h"
#include <iostream>
#include <queue>
#include <QDebug>
#include <QLinkedList>
#include <time.h>
#include <QStack>
#include "comparisongreedy.h"
#include "comparisonastar.h"

using namespace std;

Search::Search(char **board,QVector<Car> states)
{
    widhtBoard=7;
    heigthBoard=7;
    Action c;
    rootNode = new Nodo(board,0,c,states,0,0,0,0);
    /*   qDebug()<<"\n";
    QVector<Action> vector=this->findActions(board, states);

    QVector<Nodo*> nodos = expand(rootNode);
    for(int i=0;i<nodos.size();i++)
    {
        qDebug() << nodos.at(i)->toString();
    }
*/
}

Search::~Search()
{
    delete rootNode;
    rootNode=0;
}

/*
  *Metodo amplitude: Busca los movimientos a realizar para llegar a la meta, esta búsqueda se realiza siempre
  *                  expandiendo el primero de la lista enlazada, es decir, el nodo mas a la izquierda del arbol
  *                  y se sigue por el nodo que este a la misma profundida sin expandir a la izquierda hasta llegar
  *                  al extremo derecho. Conforme se expande un nodo este es removido de la lista y sus hijos agregados
  *                  a esta. Se para la búsqueda cuando se ha llegado a la meta.
  *Entrada: No tiene parametros de entrada.
  *Salida: retorna un vector de Action donde se indica como deben ser los movimientos para alcalzar la meta.
*/
QVector<Action> Search::amplitude()
{
    double timeStart = clock();

    QLinkedList<Nodo*> structAmplitude;
    amoutExpandSearch=1;

    structAmplitude.append(rootNode);
    while(!isGoal(structAmplitude.first()->getStates(),structAmplitude.first()->getBoard()))
    {
        Nodo* firstTemp = structAmplitude.first();
        QVector<Nodo*> childrenNodos = expand(firstTemp);
        amoutExpandSearch++;
        for(int i=0; i<childrenNodos.size(); i++)
        {
            structAmplitude.append(childrenNodos.at(i));
        }

        structAmplitude.removeFirst();
    }

    QVector<Action> actions = buildPath(structAmplitude.first());
    depthSearch = structAmplitude.first()->getDepth();
    totalCost += structAmplitude.first()->getG();

    double timeEnd = clock();
    totalTime = (timeEnd - timeStart)/CLOCKS_PER_SEC;

    return actions;
}

/*
  *Metodo depth: Busca el conjunto de movimientos a seguir para alcanzar la meta, la busqueda se realiza
  *              expandiendo siempre el nodo mas a la izquierda hasta alcanzar un nodo que cumpla con ser
  *              meta.
  *Entrada: No tiene parametros de entrada.
  *Salida: retorna un vector de Action donde se indica como deben ser los movimientos para alcalzar la meta.
*/
QVector<Action> Search::depth()
{

    double timeStart = clock();

    QStack<Nodo*> structDepth;
    amoutExpandSearch=1;

    structDepth.push(rootNode);

    while(!isGoal(structDepth.top()->getStates(),structDepth.top()->getBoard()))
    {

        Nodo* firstTemp = structDepth.pop();
        QVector<Nodo*> childrenNodos = expand(firstTemp);
        amoutExpandSearch++;
        for(int i=childrenNodos.size()-1; i>=0; i--)
        {
            structDepth.push(childrenNodos.at(i));
        }
    }

    QVector<Action> actions = buildPath(structDepth.top());
    depthSearch = structDepth.top()->getDepth();
    totalCost += structDepth.top()->getG();

    double timeEnd = clock();
    totalTime = (timeEnd - timeStart)/CLOCKS_PER_SEC;

    return actions;

}

/*
  *Metodo uniformCost: Busca los movimientos necesarios para llegar a la meta- realizando la menor cantidad posible
  *                    de movimientos- menor costo. se expande por el nodo que tenga menor costo sin importar la
  *                    profundidad en el arbol para elegir el de menor costo se compara el atributo g.
  *Entrada: No tiene parametros de entrada.
  *Salida: retorna un vector de Action donde se indica como deben ser los movimientos para alcalzar la meta.
*/
QVector<Action> Search::uniformCost()
{
    double timeStart = clock();

    priority_queue<Nodo*, vector<Nodo*>, ComparisonUniformCost> structUniformCost;
    amoutExpandSearch=1;

    structUniformCost.push(rootNode);
    while(!isGoal(structUniformCost.top()->getStates(),structUniformCost.top()->getBoard()))
    {
        Nodo* firstTemp = structUniformCost.top();
        //qDebug() << firstTemp->toString();
        QVector<Nodo*> childrenNodos = expand(firstTemp);
        structUniformCost.pop();
        amoutExpandSearch++;
        for(int i=0; i<childrenNodos.size(); i++)
        {
            structUniformCost.push(childrenNodos.at(i));
        }
    }

    QVector<Action> actions = buildPath(structUniformCost.top());
    depthSearch = structUniformCost.top()->getDepth();
    totalCost += structUniformCost.top()->getG();

    double timeEnd = clock();
    totalTime = (timeEnd - timeStart)/CLOCKS_PER_SEC;

    return actions;
}

/*
  *Metodo greedy:Busca los movimientos necesarios para llegar a la meta- realizando la menor cantidad posible
  *                    de movimientos- menor costo. se expande por el nodo que tenga menor costo sin importar la
  *                    profundidad en el arbol para elegir el de menor costo se compara el atributo h.
  *Entrada: No tiene parametros de entrada.
  *Salida: retorna un vector de Action donde se indica como deben ser los movimientos para alcalzar la meta.
*/
QVector<Action> Search::greedy()
{
    double timeStart = clock();

    priority_queue<Nodo*, vector<Nodo*>, ComparisonGreedy> structGreedy;
    amoutExpandSearch=1;

    structGreedy.push(this->rootNode);

    while(!this->isGoal(structGreedy.top()->getStates(), structGreedy.top()->getBoard()))
    {
        Nodo* firstTemp = structGreedy.top();
        QVector<Nodo*> childrenNodos = expand(firstTemp);
        structGreedy.pop();
        amoutExpandSearch++;
        for(int i=0; i<childrenNodos.size(); i++)
        {
            structGreedy.push(childrenNodos.at(i));
        }
    }

    QVector<Action> actions = buildPath(structGreedy.top());
    depthSearch = structGreedy.top()->getDepth();
    totalCost += structGreedy.top()->getG();


    double timeEnd = clock();
    totalTime = (timeEnd - timeStart)/CLOCKS_PER_SEC;

    return actions;
}

/*
  *Metodo aStar: Busca los movimientos necesarios para llegar a la meta- realizando la menor cantidad posible
  *                    de movimientos- menor costo. se expande por el nodo que tenga menor costo sin importar la
  *                    profundidad en el arbol para elegir el de menor costo se compara el atributo f.
  *Entrada: No tiene parametros de entrada.
  *Salida: retorna un vector de Action donde se indica como deben ser los movimientos para alcalzar la meta.
*/
QVector<Action> Search::aStar()
{
    double timeStart = clock();

    priority_queue<Nodo*, vector<Nodo*>, ComparisonAStar> structAStar;
    amoutExpandSearch=1;

    structAStar.push(this->rootNode);

    while(!this->isGoal(structAStar.top()->getStates(), structAStar.top()->getBoard()))
    {
        Nodo* firstTemp = structAStar.top();
        QVector<Nodo*> childrenNodos = expand(firstTemp);
        structAStar.pop();
        amoutExpandSearch++;
        for(int i=0; i<childrenNodos.size(); i++)
        {
            structAStar.push(childrenNodos.at(i));
        }
    }

    QVector<Action> actions = buildPath(structAStar.top());
    depthSearch = structAStar.top()->getDepth();
    totalCost += structAStar.top()->getG();


    double timeEnd = clock();
    totalTime = (timeEnd - timeStart)/CLOCKS_PER_SEC;

    return actions;

}

/*
  *Metodo findActions: Encuentra todas las acciones(movimientos) posibles que pueden realizar los carros
  *                    dado un estado del tablero.
  *Entradas: board que es el tablero, y la referencia al vector de Car del tablero.
  *Salida: el vector de Action con todos los movimientos posibles de hacer.
*/
QVector<Action> Search::findActions(char** board, QVector<Car> &states)
{
    QVector<Action> actions;

    Car::directionValue dir;
    int x;
    int y;

    for(int i=0; i<states.size();i++){

        dir = ((Car)states.at(i)).getDirection();
        x = ((Car)states.at(i)).getPosition().x();
        y = ((Car)states.at(i)).getPosition().y();
        int size = ((Car)states.at(i)).getSize();
        char idCar= ((Car)states.at(i)).getId();

        if(dir == Car::Horizontal){

            int movingLeft = findFreeSpace(x,y,Action::Izquierda,board);
            //Como vamos a movernos hacia la derecha aumentamos en y el tamanio del carro -1
            int movingRigth = findFreeSpace(x,y+(size-1),Action::Derecha,board);

            /* qDebug()<<"dir: " << idCar;
            qDebug()<<"left: "<< movingLeft;
            qDebug()<< "rigth: "<< movingRigth;*/

            for(int i=1; i<=movingLeft; i++)
            {
                Action actionPossible(idCar, Action::Izquierda, i);
                actions.append(actionPossible);
            }
            for(int i=1; i<=movingRigth; i++)
            {
                Action actionPossible(idCar, Action::Derecha, i);
                actions.append(actionPossible);
            }

        }else   if(dir == Car::Vertical){

            int movingUp = findFreeSpace(x,y,Action::Arriba,board);
            //Como vamos a movernos hacia abajo aumentamos en x el tamanio del carro -1
            int movingDown = findFreeSpace(x+(size-1),y,Action::Abajo,board);

            for(int i=1; i<=movingUp; i++)
            {
                Action actionPossible(idCar, Action::Arriba, i);
                actions.append(actionPossible);
            }
            for(int i=1; i<=movingDown; i++)
            {
                Action actionPossible(idCar, Action::Abajo, i);
                actions.append(actionPossible);
            }

            /*qDebug()<<"dir: " << idCar;
            qDebug()<<"up: "<< movingUp;
            qDebug()<< "down: "<< movingDown;*/
        }
    }
    return actions;
}

/*
  *Metodo testPosition: valida si una posicion es valida en el tablero.
  *Entradas: x que representa la columna y y que representa la fila de la posicion.
  *Salida: el booleano que muestra si es valida(true) o no (false) la posicion testeada.
*/
bool Search::testPosition(int x, int y)
{
    bool test = false;

    if(x >= 0 && x < widhtBoard)
        if((y >= 0) && (y < heigthBoard))
            test = true;

    return test;
}

/*
  *Metodo findFreeSpace: Encuentra cuantas posiciones es posible moverse dada una accion(direccion de movimiento).
  *Entradas: x que representa la columna y y la fila, moving representa la direccion del movimiento y copy el tablero.
  *Salida: el valor entero de la cantidad maxima de casillas que se puede mover el carro en dicha direccion.
*/
int Search::findFreeSpace(int x, int y, Action::move moving, char **copy){

    int movingPossibles = 0;
    int xPossible = 0;
    int yPossible = 0;

    if(moving == Action::Izquierda){

        yPossible = y-1;
        while(testPosition(x,yPossible) && (copy[x][yPossible] == '0')){

            movingPossibles++;
            yPossible--;
        }
    }else if(moving == Action::Arriba){

        xPossible = x-1;

        while(testPosition(xPossible,y) && (copy[xPossible][y] == '0')){

            movingPossibles++;
            xPossible--;
        }
    }else if(moving == Action::Derecha){

        yPossible = y+1;

        while(testPosition(x,yPossible) && (copy[x][yPossible] == '0')){

            movingPossibles++;
            yPossible++;
        }
    }else if(moving == Action::Abajo){

        xPossible = x+1;

        while(testPosition(xPossible,y) && (copy[xPossible][y] == '0')){

            movingPossibles++;
            xPossible++;
        }
    }

    return movingPossibles;
}

/*
  *Metodo applyAction: aplica una accion a un nodo y retorna el nodo resultante de aplicar dicha accion.
  *Entradas: nodo el nodo a aplicar la accion, action la referencia de la accion a aplicar.
  *Salida: el nodo hijo. resultado de aplicar la accion al nodo padre.
*/
Nodo* Search::applyAction(Nodo *nodo,Action &action)
{

    char letter = action.getIdCar();
    QVector<Car> carStates = nodo->getStates();
    int index = searchIndexCar(letter,carStates);

    Car carModifyState = nodo->getStates().at(index);
    Car newCarUpdate;



    Action::move dirAction = action.getDirection();

    int x = carModifyState.getPosition().x();
    int y = carModifyState.getPosition().y();
    int moving = action.getMoving();



    QPoint newStartPoint(x,y);

    if(dirAction == Action::Derecha){

        newStartPoint.setY(y+moving);

    }else     if(dirAction == Action::Izquierda){

        newStartPoint.setY(y-moving);

    }else     if(dirAction == Action::Abajo){
        newStartPoint.setX(x+moving);

    }else     if(dirAction == Action::Arriba){

        newStartPoint.setX(x-moving);

    }

    newCarUpdate.setId(letter);
    newCarUpdate.setPosition(newStartPoint);
    newCarUpdate.setSize(carModifyState.getSize());
    newCarUpdate.setDirection(carModifyState.getDirection());

    QVector<Car> states =  nodo->getStates();
    states.replace(index,newCarUpdate);

    char** board = nodo->getBoard();

    deleteCar(board,carModifyState);
    insertCar(board,newCarUpdate);

    // Crear Nodo

    int g = nodo->getG() + moving;
    // Actualizar Heuristica debe ir la funcion h(nodo)
    int h = this->heuristicSearch(nodo->getStates(), nodo->getBoard());
    int  f = g + h;
    int depth = nodo->getDepth() + 1;

    Nodo *sonNode = new Nodo(board,nodo,action,states,g,h,f,depth);

    return sonNode;
}

/*
  *Metodo deleteCar: Borra la ocurrencia de un carro en el tablero.
  *Entradas: board el tablero, y car referencia al carro a eliminar del tablero.
  *Salida: la modificacion en el tablero.
*/
void Search::deleteCar(char** board, Car &car)
{
    Car::directionValue dir = car.getDirection();
    int x = car.getPosition().x();
    int y = car.getPosition().y();
    int size = car.getSize();

    if(dir == Car::Horizontal)
    {

        for(int j=0; j<size;j++){

            board[x][y+j] = '0';

        }

    }else if(dir == Car::Vertical)
    {

        for(int i=0; i<size;i++){

            board[x+i][y] = '0';
        }
    }
}

/*
  *Metodo insertCar: Ingresa un carro en el tablero.
  *Entradas: board el tablero, car la referencia del carro a ingresar en el tablero.
  *Salida: la modificacion del tablero con la ocurrencia del carro ingresado.
*/
void Search::insertCar(char** board, Car &car)
{
    Car::directionValue dir = car.getDirection();
    int x = car.getPosition().x();
    int y = car.getPosition().y();
    int size = car.getSize();
    char letter = car.getId();

    if(dir == Car::Horizontal)
    {

        for(int j=0; j<size;j++){

            board[x][y+j] = letter;

        }

    }else if(dir == Car::Vertical)
    {

        for(int i=0; i<size;i++){

            board[x+i][y] = letter;
        }
    }
}

/*
  *Metodo searchIndexCar: Busca el indice del carro cuyo id es dado en el vector de estados que tambien es dado.
  *Entradas: letter id del carro a buscar, states referencia del vector de carros.
  *Salida: el indice del carro si este se encontraba en el vector sino -1.
*/
int Search::searchIndexCar(char letter, QVector<Car> &states)
{
    for(int i=0;i<states.size();i++)
    {
        if(letter == states[i].getId())
        {
            return i;
        }
    }

    return -1;
}

/*
  *Metodo expand: Aplica los operadores al nodo padre y gurada los hijos en el vector de retorno.
  *Entrada: nodo, el nodo padre que será expandido.
  *Salida: el vector de nodos hijos que resulta de aplicar los operadores al nodo padre.
*/
QVector<Nodo*> Search::expand(Nodo* nodo)
{
    char** board = nodo->getBoard();
    QVector<Car> states = nodo->getStates();

    QVector<Action> actions = findActions(board,states);
    QVector<Nodo*> nodes;

    for(int i=0;i<actions.size();i++)
    {
        Action a = actions.at(i);
        Nodo* b = applyAction(nodo,a);

        Nodo* test = b->getDad();
        bool findCycle = false;
        while(test->getDepth() != 0){

            if((b->getStates() == test->getStates()))
            {
                findCycle = true;
                break;
            }else{
                test = test->getDad();
            }

        }

        if(!findCycle){
            if((b->getStates() == test->getStates()))
            {
                findCycle = true;
            }
        }


        if(!findCycle){
            nodes.append(b);
        }else{
          delete b;
        }


      /*  if((nodo->getDad()==0) || (b->getStates() != nodo->getDad()->getStates()))//evitar devolverse
        {

            nodes.append(b);
        } else{
           delete b;
        }
*/

    }
    return nodes;
}

/*
  *Metodo isGoal: Valida si un estado de un tablero es meta o no, dado el estado de los carros y el tablero.
  *Entradas: states que representa el estado de los carros en el tablero, y board que es el tablero.
  *Salida: booleano que valida si el estado del tablero es meta.
*/
bool Search::isGoal(QVector<Car> states, char** board)
{
    char letter = 'A';
    int index = searchIndexCar(letter,states);

    int x = ((Car)states.at(index)).getPosition().x();
    int y = ((Car)states.at(index)).getPosition().y() +2;
    int yIni = y;

    while(y<widhtBoard){
        if(board[x][y] != '0'){
            return false;
        }
        y++;
    }

    totalCost =  y-yIni;
    return true;
}

/*
  *Metodo buildPath: Recibe el ultimo nodo del camino, y agregando las acciones (movimientos) que se tienen que hacer
  *                  para llegar hasta el, esto lo hace preguntando por el nodo padre, hasta que el nodo padre sea la
  *                  raiz del árbol.
  *Entrada: goalNodo, nodo que corresponde a la meta.
  *Salida: El vector de Action que representan todos los movimientos que se deben hacer para llegar a la meta.
*/
QVector<Action> Search::buildPath(Nodo* goalNodo)
{
    QVector<Action> path;
    char letter = 'A';
    QVector<Car> states = goalNodo->getStates();
    int index = searchIndexCar(letter,states);
    int y = ((Car)states.at(index)).getPosition().y() +2;

    int moving = widhtBoard-y;

    Action action('A',Action::Derecha,moving);

    Nodo* actual= goalNodo;
    path.append(action);
    while (actual->getDad() != 0)
    {
        path.prepend(actual->getAction());
        actual = actual->getDad();
    }

    return path;
}

/*
  *Metodo getDepthSearch: retorna la profundiad alcanzada para obtener la meta.
  *Entrada: No tiene parametros de entrada.
  *Salida: el entero que representa la profundidad donde se alcanzo la meta.
*/
int Search::getDepthSearch()
{
    return depthSearch;
}

/*
  *Metodo getDepthSearch: Retorna la cantidad de nodos expandidos.
  *Entrada: No tiene parametros de entrada.
  *Salida: El entero que representa la cantidad de nodos expandidos.
*/
int Search::getAmoutExpandSearch()
{
    return amoutExpandSearch;
}

/*
  *Metodo getDepthSearch: Retorna el costo acumulado de un nodo en el arbol de busqueda.
  *Entrada: No tiene parametros de entrada.
  *Salida: El entero que representa el costo total hasta ese momento, nos interesa el de la solucion.
*/
int Search::getTotalCost()
{
    return totalCost;
}
/*
  *Metodo getDepthSearch: Retorna el tiempo tomado para encontrar la meta.
  *Entrada: No tiene parametros de entrada.
  *Salida: El double que representa la cantidad de tiempo que demoró el algoritmo en encontrar la meta.
*/
double Search::getTotalTime()
{
    return totalTime;
}

/*
  *Metodo heuristicSearch: Calcula la heuristica dado un estado del tablero. Heuristica, cantidad de obstaculos
  *                        entre el carro A y la meta.
  *Entradas: states vector de los estados de los carros  y board el tablero.
  *Salida: El valor de la heuristica para ese estado del tablero.
*/
int Search::heuristicSearch(QVector<Car> states, char** board)
{
    //Amount of obstacles between car 'A' and goal.
    char letter = 'A';
    int index = searchIndexCar(letter,states);
    int heuristic=0;

    int x = ((Car)states.at(index)).getPosition().x();
    int y = ((Car)states.at(index)).getPosition().y() +2; // car location.

    while(y<widhtBoard){
        if(board[x][y] != '0'){
            heuristic++;
        }
        y++;
    }

    return heuristic;
}
