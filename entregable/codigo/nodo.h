/*
*Nombre archivo: nodo.h
*Autor: Maria Andrea Cruz Blandon, Yerminson Doney Gonzalez Muñoz y Cristian Leonardo Ríos López
*Fecha creación: 25 Septiembre de 2011
*Fecha última actualización: 17 Dicciembre de 2011
*Descripción: La clase Nodo modela un dodo de un árbol de búsqueda, cada nodo contiene el
    estado actual de la búsqueda(en este caso un tablero, su ancho y alto, y el estado de
    los elementos que en él se encuetran), una referencia al nodo padre, o sea del nodo
    que proviene este, una acción, que es la acción con la cual se llegó a ese estado,
    el costo de llegar a ese estado(g), el resultado de aplicar una heuristica a ese nodo(h),
    la suma del costo y la heuritica(f), y la profundidad en el árbol de búsqueda a la que
    se encuentra  el nodo.
*Universidad del Valle
*/

#ifndef NODO_H
#define NODO_H

#include "action.h"
#include "car.h"
#include <QVector>

class Nodo
{
private:
    char **board;
    Nodo *dad;
    Action action;
    QVector<Car> states;
    int g;
    int h;
    int f;
    int depth;
    int width;//tamano del tablero
    int height;

public:
    Nodo();
    Nodo(char **board,Nodo *dad, Action action, QVector<Car> states, int g, int h, int f, int depth);
    ~Nodo();
    QString toString();

    void setBoard(char **board);
    void setDad(Nodo *dad);
    void setAction(Action action);
    void setStates(QVector<Car> states);
    void setG(int g);
    void setH(int h);
    void setF(int f);
    void setDepth(int depth);

    char** getBoard();
    Nodo* getDad();
    Action getAction();
    QVector<Car> getStates();
    int getG();
    int getH();
    int getF();
    int getDepth();
};

#endif // NODO_H
