/*
*Nombre archivo: nodo.cpp
*Autor: Maria Andrea Cruz Blandon, Yerminson Doney Gonzalez Muñoz y Cristian Leonardo Ríos López
*Fecha creación: 25 Septiembre de 2011
*Fecha última actualización: 17 Dicciembre de 2011
*Universidad del Valle
*/

#include "nodo.h"

Nodo::Nodo()
{
    this->height=0;
    for(int i=0; i<this->height; i++)
    {
        this->board[i]=0;
    }
    this->board=0;

    this->dad=0;
}

Nodo::Nodo(char **board,Nodo *dad, Action action, QVector<Car> states, int g, int h, int f, int depth)
{
    this->board = board;
    this->dad = dad;
    this->action = action;
    this->states = states;
    this->g = g;
    this->h = h;
    this->f = f;
    this->depth = depth;
    this->width= 7;
    this->height= 7;
}

Nodo::~Nodo()
{
   for(int i=0; i<this->height; i++)
   {
       delete this->board[i];
       this->board[i]=0;
   }

   delete this->board;
   this->board=0;

   this->dad=0;
}

/*
Método setBoard: cambia el tablero que representa el estado al que referencia el nodo
Entradas: un puntero doble a char que es la referencia al tablero
Salida: no tiene salida, solo afecta el estado del nodo
*/
void Nodo::setBoard(char **board)
{
   this->board= board;
}

/*
Método setDad: cambia el padre del nodo
Entradas: un puntero a un nodo
Salida: no tiene salida, solo afecta el estado del nodo
*/
void Nodo::setDad(Nodo *dad)
{
    this->dad= dad;
}

/*
Método setAction: cambia la acción con la que se llego al nodo
Entradas: un objeto de tipo Action
Salida: no tiene salida, solo afecta el estado del nodo
*/
void Nodo::setAction(Action action)
{
    this->action= action;
}

/*
Método setStates: cambia los estados(carros en un tablero) del nodo
Entradas: un vector de objetos tipo Car
Salida: no tiene salida, solo afecta el estado del nodo
*/
void Nodo::setStates(QVector<Car> states)
{
    this->states= states;
}

/*
Método setG: cambia el costo con el que se llego al nodo
Entradas: un entero que representa el nuevo costo
Salida: no tiene salida, solo afecta el estado del nodo
*/
void Nodo::setG(int g)
{
    this->g= g;
}

/*
Método setH: cambia el costo de la heuristica del nodo
Entradas: un entero que representa el nuevo costo de la heuristica
Salida: no tiene salida, solo afecta el estado del nodo
*/
void Nodo::setH(int h)
{
    this->h=h;
}

/*
Método setF: cambia el costo(costo real + heuristica)con el que se llego al nodo
Entradas: un entero que representa el nuevo costo
Salida: no tiene salida, solo afecta el estado del nodo
*/
void Nodo::setF(int f)
{
    this->f=f;
}

/*
Método setDepth: cambia la profundidad a la que se encuentra el nodo en el árbol de búsqueda
Entradas: un entero que representa la nueva profundiad
Salida: no tiene salida, solo afecta el estado del nodo
*/
void Nodo::setDepth(int depth)
{
    this->depth=depth;
}

/*
Método getBoard: crea una copia del tablero
Entradas: no tiene entradas
Salida: un puntero doble a char que representa el tablero
*/
char** Nodo::getBoard()
{
    char** copyBoard= new char*[this->height];
    for(int i=0; i< this->height; i++)
    {
        copyBoard[i]= new char[this->width];
    }

    for(int i=0; i<this->height; i++)
        for(int j=0; j<this->width; j++)
            copyBoard[i][j]= this->board[i][j];

    return copyBoard;
}

/*
Método getDad: obtiene uan ferecencia al padre del nodo
Entradas: no tiene entradas
Salida: un puntero a un nodo
*/
Nodo* Nodo::getDad()
{
    return this->dad;
}

/*
Método getAction: obtiene la accion con la que se llego al nodo
Entradas: no tiene entradas
Salida: un objeto de tipo Action
*/
Action Nodo::getAction()
{
    return this->action;
}

/*
Método getStates: obtiene los estados(objetos Car que representan los carros) del nodo
Entradas: no tiene entradas
Salida: un vector que contiene objetos de tipo Car
*/
QVector<Car> Nodo::getStates()
{
    return this->states;
}

/*
Método getG: obtiene el costo con el que se llego al nodo
Entradas: no tiene entradas
Salida: un entero que representa el costo
*/
int Nodo::getG()
{
    return this->g;
}

/*
Método getH: obtiene el costo de la heuristica del nodo
Entradas: no tiene entradas
Salida: un entero que representa el costo de la heuristica
*/
int Nodo::getH()
{
    return this->h;
}

/*
Método getF: obtiene el costo(costo real + heuristica) con el que se llego al nodo
Entradas: no tiene entradas
Salida: un entero que representa el costo
*/
int Nodo::getF()
{
    return this->f;
}

/*
Método getDepth: obtiene la profundidad del nodo
Entradas: no tiene entradas
Salida: un entero que representa la profundidad
*/
int Nodo::getDepth()
{
    return this->depth;
}

/*
Método toString: obtiene los atributos del nodo y los convierte en un solo String
Entradas: no tiene entradas
Salida: un String que representa el estdo del nodo
*/
QString Nodo::toString()
{
    QString nodo = "Tablero \n\n";

    for(int i=0;i<width;i++)
    {
        for(int j=0;j<height;j++)
        {
            nodo += QString(board[i][j]) + "  ";
        }
        nodo += "\n";
    }

    nodo += "\n Accion: " + action.toString();
    nodo += "\n\n Carritos \n\n";
    for(int i=0;i<states.size();i++)
    {
        nodo += states[i].toString() + "\n";
    }

    nodo += " g: " + QString::number(g) + " h: " + QString::number(h) + " f: " + QString::number(f) + " profundidad: " + QString::number(depth);

    return nodo;
}
