#ifndef COMPARISONGREEDY_H
#define COMPARISONGREEDY_H

#include "nodo.h"

class ComparisonGreedy
{
public:
    bool operator()(Nodo* nodo1, Nodo* nodo2)
    {
        if(nodo1->getH() >= nodo2->getH())
            return true;
        else
            return false;

    }
};

#endif // COMPARISONGREEDY_H
