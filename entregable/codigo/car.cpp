/*

* Nombre archivo : car.cpp
* Nombre de la clase : Car
* Autor: Maria Andrea Cruz Blandon
         Cristian Leonardo Rios
         Yerminson Doney Gonzalez Muñoz
* Fecha de creación: Septiembre del 2011
* Fecha de modificación: Diciembre del 2011
*
* Universidad del Valle
*/

#include "car.h"
#include <QString>

Car::Car()
{}

Car::Car(char id, QPoint position, int size, directionValue direction)
{
    this->position = position;
    this->size = size;
    this->direction = direction;
    this->id = id;
}

/*
* Metodo setPosition: Permite cambiar la posicion de un carro es decir sus coordenadas x y y, representadas por un QPoint
* Entradas: Un QPoint que representa el punto en el cual se va a colocar el carro.
* Salida: No hay salida, ya que solo representa un cambio en el atributo position del carro.
*/
void Car::setPosition(QPoint position)
{
    this->position = position;
}

/*
* Metodo setSize: Permite cambiar el tamaño de un carro
* Entradas: Un numero representando el nuevo tamaño del carro.
* Salida: No hay salida, ya que solo representa un cambio en el atributo size del carro.
*/
void Car::setSize(int size)
{
    this->size = size;
}

/*
* Metodo setDirection : Permite cambiar la direccion en la cual esta posicionado el carro(vertical u horizontal).
* Entradas: Una direccion(directionValue) representando la nueva direccion del carro.
* Salida: No hay salida, ya que solo representa un cambio en el atributo direction del carro.
*/
void Car::setDirection(directionValue direction)
{
    this->direction = direction;
}

/*
* Metodo setId: Permite cambiar el identificador de un carro, asignandole una nueva letra.
* Entradas: Un caracter que representa la letra por la cual se va a modificar el carro .
* Salida: No hay salida, ya que solo representa un cambio en el atributo id del carro.
*/
void Car::setId(char id)
{
    this->id = id;
}

/*
* Metodo getPosition: Permite obtener la posicion en la cual se encuentra el carro actualmente.
* Entradas: No hay entrada, ya que solo es una consulta.
* Salida: Un QPoint representando la posicion en x y y del carro.
*/
QPoint Car::getPosition() const
{
    return position;
}

/*
* Metodo getSize: Permite obtener el tamaño de un carro.
* Entradas: No hay entrada ya que solo es una consulta.
* Salida: Un numero que representa el tamaño del carro.
*/
int Car::getSize()
{
    return size;
}

/*
* Metodo getDirection: Permite obtener la posicion en la cual esta posicionado el carro.
* Entradas: No hay entrada, ya que solo es una consulta.
* Salida: Un directionValue(direccion) que representa la direccion en la cual se enecuentra el carro.
*/
Car::directionValue Car::getDirection()
{
    return direction;
}

/*
* Metodo getID: Permite obtener el id de un carro.
* Entradas: No hay entrada, ya que solo es un metodo de consulta.
* Salida: Un caracter que representa la letra que tiene el carro.
*/
char Car::getId()
{
    return id;
}

/*
* Metodo toString: Permite constuir una cadena de caracteres representando toda la informacion de los atributos.
* Entradas: No hay entrada, ya que solo es un metodo de salida.
* Salida: Un QString que agrupa de manera organizada los atributos de un carro en una cadena de caracteres.
*/
QString Car::toString(){

    QString descriptionCar;

    descriptionCar = "Id : "+QString(id)+ " - Position : ("+ QString::number(position.x())+","+QString::number(position.y())+") - Direction: "+QString::number(direction)+" - Size: "+QString::number(size);

    return descriptionCar;
}

/*
* Metodo ==: Permite sobrecargar el operador == para realizar compraraciones entre carros.
* Entradas: Un car que representa el carro contra el cual se va a realizar la comparacion
* Salida: Un booleano representando si se cumple o no que que un carro es igual a otro.
*/
bool Car::operator ==(Car &car)
{
    if((this->id != car.getId()) || (this->direction != car.getDirection()) || (this->position != car.getPosition()) || (this->size != car.getSize()))
        return false;

    return true;
}

