/*

* Nombre archivo : car.h
* Nombre de la clase : Car
* Autor: Maria Andrea Cruz Blandon
         Cristian Leonardo Rios
         Yerminson Doney Gonzalez Muñoz
* Fecha de creación: Septiembre del 2011
* Fecha de modificación: Diciembre del 2011
*
* Descripción: La clase Car modela el estado de un carro en un determinado momento
* del árbol de busqueda, cada carro se representa mediante una posicion en el ambiente,
* un tamaño del carro que puede ser 2 o 3  , una direccion en la cual esta posicionado el
* carro en el ambiente que puede ser horizontal o vertical y una letra que permite identificar
* un carro de los demas.
*
* Universidad del Valle
*/



#ifndef CAR_H
#define CAR_H

#include <QPoint>

class Car
{
public:
    enum directionValue {Vertical, Horizontal, Ninguna};

    Car();
    Car(char id, QPoint startPosition, int size, directionValue direction);
    void setPosition(QPoint position);
    void setSize(int size);
    void setDirection(directionValue direction);
    void setId(char id);

    QPoint getPosition() const;
    int getSize();
    directionValue getDirection();
    char getId();
    QString toString();

    bool operator ==(Car &car);

private:
    QPoint position;
    int size;
    directionValue direction;
    char id;
};

#endif // CAR_H
