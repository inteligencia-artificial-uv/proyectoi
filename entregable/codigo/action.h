/*

* Nombre archivo : action.h
* Nombre de la clase : Action
* Autor: Maria Andrea Cruz Blandon
         Cristian Leonardo Rios
         Yerminson Doney Gonzalez Muñoz
* Fecha de creación: Septiembre del 2011
* Fecha de modificación: Diciembre del 2011
*
*
* Descripción: La clase Action modela una accion que tiene posibilidad de realizar un carro
* en un determinado estado del árbol de búsqueda, cada accion contiene el identificador del
* carro que puede realizar la accion, la direccion en la cual se movera el carro que esta representada
* por los posibles operadores que se pueden aplicar en ese momento (izquierda o derecha si es un carro horizontal
* o arriba o abajo si es un carro vertical)y la cantidad es decir el numero de espacios que se desplazara el carro.
*
* Universidad del Valle
*/

#ifndef ACTION_H
#define ACTION_H

#include <QString>

class Action
{
public:
        enum move{Izquierda, Arriba, Derecha,  Abajo};

    Action();
    Action(char idCar,move direction,int moving);
    void setIdCar(char idCar);
    void setDirection(move direction);
    void setMoving(int moving);

    char getIdCar();
    move getDirection();
    int getMoving();
    QString toString();

private:
    char idCar;
    move direction;
    int moving;
};

#endif // ACTION_H
