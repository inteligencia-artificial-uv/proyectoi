#-------------------------------------------------
#
# Project created by QtCreator 2011-09-04T12:14:20
#
#-------------------------------------------------

QT       += core gui

TARGET = proyecto1
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    scene.cpp \
    car.cpp \
    reader.cpp \
    nodo.cpp \
    action.cpp \
    search.cpp \
    item.cpp

HEADERS  += mainwindow.h \
    scene.h \
    item.h \
    car.h \
    reader.h \
    comparisonastar.h \
    comparisongreedy.h \
    comparisonuniformcost.h \
    nodo.h \
    action.h \
    search.h

RESOURCES += \
    proyecto1.qrc





















