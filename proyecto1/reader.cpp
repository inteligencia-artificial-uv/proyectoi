/*
  *Nombre archivo: reader.cpp
  *Autor: María Andrea Cruz Blandón
  *       Yerminson Doney Gonzalez Muñoz
  *       Cristian Leonardo Ríos López
  *Fecha de creación: Septiembre 2011
  *Fecha de ultima modificación: Diciembre 2011
  *Universidad del Valle
*/

#include "reader.h"
#include <QFile>
#include <QStringList>
#include <QTextStream>
#include <QDebug>
#include <QPoint>

Reader::Reader()
{
    this->widht=7;
    this->height=7;
    this->board= new char*[this->height];

   for(int i=0; i<this->height; i++)
   {
       this->board[i]= new char[this->widht];
   }
}

Reader::~Reader()
{
    for(int i=0;i<this->height; i++)
    {
        delete this->board[i];
        this->board[i]=0;
    }
    delete this->board;
    this->board=0;
}

/*
  *Metodo load: Este metodo lee el archivo cuyo nombre es dado, y da valores a las celdas del tablero y establece los numeros
  *             de las filas y las columnas de dicho tablero.
  *Entrada: rute que es un string del nombre del archivo que contiene la configuracion.
  *Salida: la modificacion de los atributos, board, heigh y width. con la información obtenida del archivo.
*/
void Reader::load(QString rute)
{
    QFile file(rute);
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        {   //qDebug("algo");
            return;
        }

        QTextStream in(&file);
        QString line;
        QStringList row;
        int i=0;
        while (!in.atEnd())
        {
            line = in.readLine();
            row = line.split(" ");

            for(int j=0; j<row.size();j++)
            {
                board[i][j] = row.at(j).at(0).toAscii();
            }
            i++;
        }

        file.close();

        this->height = i; //por la altura se conocen las filas
        this->widht = row.size(); //por la anchura se conocen las filas

}

/*
  *Metodo getStates: retorna el vector de estados del tablero.
  *Entrada: No tiene parametros de entrada.
  *Salida: el vector states de Car que contiene el estado de cada carro en el tablero.
*/
QVector<Car> Reader::getStates()
{
    return states;
}

/*
  *Metodo getObstacles: retorna el vector de puntos donde estan los obstaculos.
  *Entrada: No tiene parametros de entrada
  *Salida: Vector de QPoint que representan los obstaculos.
*/
QVector<QPoint> Reader::getObstacles()
{
    return obstacles;
}

/*
  *Metodo createStates: crea los vectores de estados, de carros y obstaculos con la información del tablero inicial.
  *Entrada: No tiene parametros de entrada.
  *Salida: la modificacion de los vectores de estados, atributos obstacles y states.
*/
void Reader::createStates()
{
    char** copy = this->getCopyBoard();

    for(int i = 0; i < height; i++)
    {
        for(int j=0; j <widht; j++)
        {
            // Encontre un  bloque lo guardo en los obstaculos
            if(copy[i][j]== '1'){

               QPoint puntoObstaculo(i,j);

               obstacles.append(puntoObstaculo);
               //Encontre un carr que puede ser A, B , C, D, E ,F y G.
            }else if(copy[i][j] > 64 && copy[i][j] < 75)
            {
                char letter = copy[i][j];
                searchCar(i,j,letter,copy);
            }
        }
    }

    /*for(int i= 0;i < states.size(); i++){

        qDebug()<< ((Car)states.at(i)).toString();
       // qDebug()<< obstacles.at(i);
    }*/

}


/*
  *Metodo searchCar: Este metodo busca como esta el carro en el tablero, en un principio no se sabe si esta horizontal o
  *                  vertical ni que tamaño tiene, por lo que este metodo realiza dicha busca y deduce las caracteristicas
  *                  apartir de la informacion que esta en el tablero copy.
  *Entrada: x que representa la columna donde esta el carro, y representa la fila, letter el id del carro y copy el tablero.
  *Salida: agrega al vector states el carro encontrado, con las caracteristicas direccion, tamaño , id y posicion.
*/
void Reader::searchCar(int x, int y,char letter,char** copy){

     int carSize = 1;
     Car::directionValue direction;
     int yPossible = y+1;
     int xPossible = x+1;


     //Encontre una coincidencia a la derecha
     if(testPosition(x,yPossible) && (copy[x][yPossible] == letter))
     {

         direction = Car::Horizontal;
         yPossible++;
         carSize++;

         //Encontre otra coincidencia un carro de tamanio 3
         if(testPosition(x,yPossible) && (copy[x][yPossible] == letter)){

             carSize++;
             QPoint pos(x,y);

             Car car(letter,pos,carSize,direction);
             states.append(car);
             pathClean(x,y,direction,carSize,copy);

             //Es un carro de tamanio 2
         }else{

             QPoint pos(x,y);
             Car car(letter,pos,carSize,direction);
             states.append(car);
            pathClean(x,y,direction,carSize,copy);
         }

        //Encontre una coincidencia hacia abajo
     }else if(testPosition(xPossible,y) && (copy[xPossible][y] == letter))
     {

         direction = Car::Vertical;
         xPossible++;
         carSize++;

         //Encontre otra coincidencia un carro de tamanio 3
         if(testPosition(xPossible,y) && (copy[xPossible][y] == letter)){

             carSize++;
             QPoint pos(x,y);

             Car car(letter,pos,carSize,direction);
             states.append(car);
             pathClean(x,y,direction,carSize,copy);
             //Es un carro de tamanio 2
         }else{

             QPoint pos(x,y);
             Car car(letter,pos,carSize,direction);
             states.append(car);
             pathClean(x,y,direction,carSize,copy);
         }

     }
 }

/*
  *Metodo pathClean: elimina las letras que corresponden a un carro en el tablero.
  *Entrada: x representa la columan donde esta el carro, y la fila, dir representa la direccion del carro,
  *         size el tamaño del carro y copy el tablero.
  *Salida: la modificacion del puntero copy, este sin la apacion de dicho carro.
*/
 void Reader::pathClean(int x, int y,Car::directionValue dir,int size,char** copy){

     if(dir == Car::Horizontal){

         for(int j=0; j<size;j++){

             copy[x][y+j] = '0';

         }

     }else if(dir == Car::Vertical){

         for(int i=0; i<size;i++){

             copy[x+i][y] = '0';
         }
     }
 }

 /*
   *Metodo getCopyBoard: retorna la informacion del tablero en una copia.
   *Entrada: No tiene parametros de entrada.
   *Salida: un tablero que es copia del tablero de la clase.
*/
char** Reader::getCopyBoard()
{
    char** boardCopy;

    boardCopy = new char*[height];

    for(int i= 0; i< height; i++)
    {
        boardCopy[i] = new char[widht];

        for(int j=0; j < widht;j++)
        {
            boardCopy[i][j] = board[i][j];
        }
    }

    return boardCopy;
}

/*
  *Metodo testPosition: verifica si una posicion es valida en el tablero.
  *Entradas: x representa la columna y y representa la fila.
  *Salida: el booleano que afirma o niega la validez de la posicion.
*/
bool Reader::testPosition(int x, int y)
{
    bool test = false;

    if(x >= 0 && x < widht)
        if((y >= 0) && (y < height))
            test = true;

    return test;
}

/*
  *Metodo getHeight: retorna el valor de las filas del tablero.
  *Entrada: No tiene parametros de entrada.
  *Salida: el entero con el valor de las filas del tablero.
*/
int Reader::getHeight()
{
    return height;
}

/*
  *Metodo getWidth: retorna el valor de las columans del tablero.
  *Entrada: No tiene parametros de entrada.
  *Salida: el entero con el valor de las columnas del tablero.
*/
int Reader::getWidth()
{
    return widht;
}
