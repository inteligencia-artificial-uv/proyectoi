/*
  *Nombre archivo: mainwindow.h
  *Autor: Cristian Leonardo Ríos López
  *Fecha creación: 20 Septiembre de 2011
  *Fecha última actualización: 17 Dicciembre de 2011
  *Universidad del Valle
*/

#include <QGraphicsView>
#include <QComboBox>
#include <QSpinBox>
#include <QPushButton>
#include <QToolBox>
#include <QButtonGroup>
#include <QWidget>
#include <QStringList>
#include <QLabel>
#include <QMessageBox>
#include <QTextEdit>
#include <QTextCodec>
#include <QHBoxLayout>
#include <QAction>
#include <QMenuBar>
#include <QToolBar>
#include <QToolButton>
#include <QFileDialog>
#include <QTextStream>

#include "mainwindow.h"
#include "scene.h"
#include "reader.h"
#include "search.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    QTextCodec *linuxCodec = QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForTr(linuxCodec);

    createActions();
    createMenus();
    createToolbars();
    createToolBox();

    scene = new Scene(7,7,this);
    scene->setSceneRect(QRectF(0, 0, 600, 500));
    view = new QGraphicsView(scene);
    connect(scene,SIGNAL(itemInserted(Item*)),this,SLOT(itemInserted(Item*)));
    connect(scene,SIGNAL(itemDelete(Item*)),this,SLOT(itemDelete(Item*)));
    scene->drawGrid();/*el tablero inicial*/

    buttonisChecked = -1; //usado para control

    QHBoxLayout *layout = new QHBoxLayout;
    layout->addWidget(view);
    layout->addWidget(toolBox);

    QWidget *widget = new QWidget;
    widget->setLayout(layout);

    setCentralWidget(widget);
    setWindowTitle(tr("Inteligencia Artificial - Proyecto 1 -Algoritmos de Busqueda - GPI"));
}

MainWindow::~MainWindow()
{
    delete scene;
    delete view;
    delete algorithmComboBox;
    delete algorithmTypeComboBox;
    delete letterComboBox;
    delete runPushButton;
    delete restartPushButton;
    delete clearPushButton;
    delete exitAction;
    delete importFileAction;
    delete exportFileAction;
    delete deleteAction;
    delete aboutAction;
    delete helpAction;
    delete fileMenu;
    delete itemMenu;
    delete helpMenu;
    delete editToolBar;
    delete fileToolBar;
    delete moreToolBar;
    delete buttonGroup;
    delete labelResultDepthTree;
    delete labelResultManyNodes;
    delete labelResultTime;
    delete labelResultCost;
    delete toolBox;
}

/*
  *Método slot about: recibe signal de un QAction, lo que abre un dialogo mostrando información
    acerca del programa y sus desarrolladores
  *Entradas: no recibe entradas
  *Salida: no tiene salida, solo afecta la interfaz
*/
void MainWindow::about()
{
    QMessageBox::about(this, tr("Acerca del proyecto 1 IA"),
                       tr("GPI: <b>G</b>estor de <b>P</b>arqueadero <b>I</b>nteligente<br><br>"
                           "<b>Desarrolladores:</b> <br><br>"
                          "Maria Andrea Cruz Blandon. - 0831816.<br>"
                          "Cristian Leonardo R&iacute;os L&oacute;pez. - 0842139.<br>"
                          "Yerminson Doney Gonzalez Mu&ntilde;oz. - 0843846.<br><br>"
                          "Desarrollado usando Qt 4.7.4 y C++."
                          ));
}

/*
  *Método slot algotithmTypeChange: recibe signal de un QCombobox representando si el tipo
    de algoritmo que se desea usar para aplicar la búsqueda(informada o no informada) a
    cambiado, según el valor que tenga carga los tipo de algoritmos según la búsqueda.
  *Entradas: recibe un String que es el valor enviado por el QCombobox
  *Salida: no tiene salida, solo afecta la interfaz
*/
void MainWindow::algotithmTypeChange(const int &value)
{
     if(value == 0)//busqueda no informada
     {
         algorithmComboBox->clear();
         algorithmComboBox->addItem(tr("Preferente por amplitud"));
         algorithmComboBox->addItem(tr("Costo uniforme"));
         algorithmComboBox->addItem(tr("Preferente por profundidad evitando ciclos"));
     }else if(value == 1)//busqueda informada
     {
         algorithmComboBox->clear();
         algorithmComboBox->addItem(tr("Avara"));
         algorithmComboBox->addItem(tr("A*"));
     }
}

/*
  *Método slot buttonGroupClicked: recibe signal de un QPushButton,correspondiente a un elemento
    gráfico que se puede añadir al tablero, cada vez que uno de estos botones es presionado
    se verifica que solo el botón presionado esta activo y se actualiza en scene el tipo
    de boton presionado y el posible identificador que tendria el elemento gráfico, esto
    con el fin de poder dibujar el elemento correcto.
  *Entradas: un entero que representa el identificador del boton presionado
  *Salida: no tiene salida, solo afecta lel estado de la interfaz
*/
void MainWindow::buttonGroupClicked(int id)
{
    QList<QAbstractButton *> buttons = buttonGroup->buttons();
    foreach (QAbstractButton *button, buttons)
    {
        if (buttonGroup->button(id) != button)
        {
            button->setChecked(false);
        }
    }
    if(id != buttonisChecked)
    {
        scene->setType(Scene::Type(id));
        buttonisChecked = id;
        QString letter = letterComboBox->currentText();
        scene->setLetterCarInserted(letter);
    }else
    {
        scene->setType(Scene::Libre);
        buttonisChecked = -1;
    }
}

/*
  *Método slot clearBoard: recibe signal de un QPushButton, esto hace que se limpie
    el tablero y nuevamente se pueda volver a cargar un nuevo estado de búsqueda
    o a crear alguno dinámicamente
  *Entradas: no recibe entradas
  *Salida: no tiene salida, solo afecta la interfaz
*/
void MainWindow::clearBoard()
{
    scene->clearBoard();

    labelResultDepthTree->setText("");
    labelResultManyNodes->setText("");
    labelResultTime->setText("");
    labelResultCost->setText("");

    runPushButton->setEnabled(true);
    restartPushButton->setEnabled(false);
}

/*
  *Método createActions: inicializa todos los atributos QAction que forman la interfaz,
    además de conectarlos con su respectivo slot de escucha de señales emitidas
  *Entradas: no recibe entradas
  *Salida: no tiene salida, ayuda en la construccion de la interfaz
*/
void MainWindow::createActions()
{
    deleteAction = new QAction(QIcon(":/images/delete.png"),tr("Eliminar"), this);
    deleteAction->setStatusTip(tr("Elimine elemento del tablero"));
    connect(deleteAction, SIGNAL(triggered()),
        this, SLOT(deleteElement()));

    exitAction = new QAction(tr("Salir"), this);
    exitAction->setStatusTip(tr("Salir de la aplicaci&oacute;n"));
    connect(exitAction, SIGNAL(triggered()), this, SLOT(close()));

    aboutAction = new QAction(tr("Acerca de"), this);
    connect(aboutAction, SIGNAL(triggered()),
            this, SLOT(about()));

    helpAction = new QAction(QIcon(":/images/help.png"),tr("Ayuda"), this);
    connect(helpAction, SIGNAL(triggered()), this, SLOT(help()));

    importFileAction = new QAction(QIcon(":/images/fileopen.png"),tr("Importar Tablero"), this);
    connect(importFileAction, SIGNAL(triggered()), this, SLOT(importFile()));

    exportFileAction = new QAction(QIcon(":/images/filesave.png"),tr("Exportar Tablero"), this);
    //exportFileAction->setEnabled(false);;
    connect(exportFileAction, SIGNAL(triggered()), this, SLOT(exportFile()));
}

/*
  *Método createMenus: inicializa todos los atributos QMenu que forman la interfaz,
    estos se construyen usando QAction
  *Entradas: no recibe entradas
  *Salida: no tiene salida, ayuda en la construccion de la interfaz
*/
void MainWindow::createMenus()
{
    fileMenu = menuBar()->addMenu(tr("Archivo"));
    fileMenu->addAction(importFileAction);
    fileMenu->addAction(exportFileAction);
    fileMenu->addSeparator();
    fileMenu->addAction(exitAction);

    itemMenu = menuBar()->addMenu(tr("Elemento"));
    itemMenu->addAction(deleteAction);

    helpMenu = menuBar()->addMenu(tr("Más"));
    helpMenu->addAction(aboutAction);
    helpMenu->addAction(helpAction);
}

/*
  *Método createToolBox: inicializa el atributo toolBox, este es el panel que se encuentra
    a la derecha de la interfaz, este panel se encuentra dividido en otros dos paneles,
    el primero es de donde se controla la ejecucion del algoritmo y el segundo es donde
    se encuentran los elementos necesarios para crear tableros dinámicos. Estos dos paneles
    se crean con métodos auxiliares.
  *Entradas: no recibe entradas
  *Salida: no tiene salida, ayuda en la construccion de la interfaz
*/
void MainWindow::createToolBox()
{
    QWidget* cWidget = createPanelControl();
    QWidget* eWidget = createPanelElements();
    toolBox = new QToolBox;
    toolBox->setSizePolicy(QSizePolicy(QSizePolicy::Maximum, QSizePolicy::Ignored));
    toolBox->setMinimumWidth(cWidget->sizeHint().width());
    toolBox->addItem(cWidget, tr("Panel de Control"));
    toolBox->addItem(eWidget, tr("Panel de Elementos"));
}

/*
  *Método createToolbars: inicializa todos los atributos QToolBar que forman la interfaz,
    estos se construyen usando QAction
  *Entradas: no recibe entradas
  *Salida: no tiene salida, ayuda en la construccion de la interfaz
*/
void MainWindow::createToolbars()
{
    editToolBar = addToolBar(tr("Edit"));
    editToolBar->addAction(deleteAction);


    fileToolBar = addToolBar(tr("File"));
    fileToolBar->addAction(importFileAction);
    fileToolBar->addAction(exportFileAction);

    moreToolBar = addToolBar(tr("More"));
    moreToolBar->addAction(helpAction);
}

/*
  *Método createElementCellWidget: permite crear un boton especial, es usado para crear los
    botones del panel de elementos, estos botones representaran cada uno de los gráficos
    que pueden añadirse.
  *Entradas: tiene 3 entradas, la primera es un String que representa el nombre del boton,
    la segunda es un QString que representa la ruta de la imagen que tendrá el boton,
    y el tercero es un Type de Scene que sera el identificador(único para cada boton)
  *Salida: un puntero a a QWidget, este QWidget es el boton
*/
QWidget* MainWindow::createElementCellWidget(const QString &text, const QString &image, const Scene::Type type)
{
    QToolButton *button = new QToolButton;
    button->setText(text);
    button->setIcon(QIcon(image));
    button->setIconSize(QSize(50, 50));
    button->setCheckable(true);
    buttonGroup->addButton(button,type);

    QGridLayout *layout = new QGridLayout;
    layout->addWidget(button, 0, 0, Qt::AlignHCenter);
    layout->addWidget(new QLabel(text), 1, 0, Qt::AlignCenter);

    QWidget *widget = new QWidget;
    widget->setLayout(layout);

    return widget;
}

/*
  *Método createPanelControl: crea el panel(QWidget) donde están todos los elementos con los
    que se controla la ejecución del algoritmo, el panel es utilizado por createToolBox
    para construir la interfaz
  *Entradas: no recibe entradas
  *Salida: un puntero a a QWidget, este QWidget es el panel
*/
QWidget* MainWindow::createPanelControl()
{
    QLabel *labelAlgorithmType = new QLabel(tr("<b>B&uacute;squeda</b>"));
    labelAlgorithmType->setAlignment(Qt::AlignCenter);
    algorithmTypeComboBox = new QComboBox;
    algorithmTypeComboBox->addItem(tr("No Informada"));
    algorithmTypeComboBox->addItem(tr("Informada"));
    connect(algorithmTypeComboBox,SIGNAL(currentIndexChanged(int)),this,SLOT(algotithmTypeChange(int)));

    QLabel *labelAlgorithm = new QLabel(tr("<b>Algoritmo</b>"));
    labelAlgorithm->setAlignment(Qt::AlignCenter);
    algorithmComboBox = new QComboBox;
    algorithmComboBox->addItem(tr("Preferente por amplitud"));
    algorithmComboBox->addItem(tr("Costo uniforme"));
    algorithmComboBox->addItem(tr("Preferente por profundidad evitando ciclos"));

    runPushButton = new QPushButton(QIcon(":/images/run.png"),tr("Ejecutar"));
    connect(runPushButton,SIGNAL(clicked()),this,SLOT(runAlgorithm()));

    restartPushButton = new QPushButton(tr("Reiniciar Tablero"));
    connect(restartPushButton,SIGNAL(clicked()),this,SLOT(restartBoard()));
    restartPushButton->setEnabled(false);

    clearPushButton = new QPushButton(tr("Limpiar Tablero"));
    connect(clearPushButton,SIGNAL(clicked()),this,SLOT(clearBoard()));

    QGridLayout *gridLayout = new QGridLayout;
    gridLayout->addWidget(labelAlgorithmType,0,0,1,4);
    gridLayout->addWidget(algorithmTypeComboBox,1,0,1,4);
    gridLayout->setRowMinimumHeight(2,15);//un poco de espacio
    gridLayout->addWidget(labelAlgorithm,3,0,1,4);
    gridLayout->addWidget(algorithmComboBox,4,0,1,4);
    gridLayout->setRowStretch(5,10);
    gridLayout->addWidget(createWidgetResults(),6,0,5,4);
    gridLayout->setRowStretch(11,10);//para que los demas widget no ocupen todo el espacio
    gridLayout->addWidget(runPushButton,12,1,1,2);
    gridLayout->addWidget(restartPushButton,13,1,1,2);
    gridLayout->addWidget(clearPushButton,14,1,1,2);

    QWidget *widgetControl = new QWidget;
    widgetControl->setLayout(gridLayout);

    return widgetControl;
}

/*
  *Método createPanelElements: crea el panel(QWidget) donde están todos los elementos con los
    que se pueden crear tableros dinámicos, el panel es utilizado por createToolBox
    para construir la interfaz
  *Entradas: no recibe entradas
  *Salida: un puntero a a QWidget, este QWidget es el panel
*/
QWidget* MainWindow::createPanelElements()
{
    buttonGroup = new QButtonGroup(this);
    buttonGroup->setExclusive(false);
    connect(buttonGroup, SIGNAL(buttonClicked(int)),
            this, SLOT(buttonGroupClicked(int)));

    QGridLayout *elementsLayout = new QGridLayout;

    elementsLayout->addWidget(createElementCellWidget(tr("Auto 2H"),
                ":/images/h2a.png",Scene::AutoDosH),0,1);

    elementsLayout->addWidget(createElementCellWidget(tr("Auto 2V"),
                ":/images/v2a.png",Scene::AutoDosV),0,2);

    elementsLayout->addWidget(createElementCellWidget(tr("Auto 3H"),
                ":/images/h3a.png",Scene::AutoTresH),1,1);

    elementsLayout->addWidget(createElementCellWidget(tr("Auto 3V"),
                ":/images/v3a.png",Scene::AutoTresV),1,2);

    elementsLayout->addWidget(createElementCellWidget(tr("Obstaculo"),
               ":/images/galaxia5.png", Scene::Obstaculo), 2, 1);

    letterAvailable.append("A");
    letterAvailable.append("B");
    letterAvailable.append("C");
    letterAvailable.append("D");
    letterAvailable.append("E");
    letterAvailable.append("F");
    letterAvailable.append("G");
    letterAvailable.append("H");
    letterAvailable.append("I");

    letterComboBox = new QComboBox;
    letterComboBox->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
    QFont font("Helvetica",28);
    letterComboBox->setFont(font);
    letterComboBox->addItems(letterAvailable);
    connect(letterComboBox,SIGNAL(currentIndexChanged(QString)),this,SLOT(letterCarChange(QString)));

    QGridLayout *layoutCombo = new QGridLayout;
    layoutCombo->addWidget(letterComboBox, 0, 0, Qt::AlignHCenter);
    layoutCombo->addWidget(new QLabel(tr("Letra")), 1, 0, Qt::AlignCenter);
    QWidget *widgetCombo = new QWidget;
    widgetCombo->setLayout(layoutCombo);

    elementsLayout->addWidget(widgetCombo,2,2);
    elementsLayout->setColumnStretch(0, 10);
    elementsLayout->setRowStretch(3, 10);
    elementsLayout->setColumnStretch(3, 10);

    QWidget *widgetElements = new QWidget;
    widgetElements->setLayout(elementsLayout);

    return widgetElements;
}

/*
  *Método slot deleteElement: recibe signal de un QAction, esto hace que se consulten
    todos lo elementos graficos que esten seleccionados y se borren del tablero.
  *Entradas: no recibe entradas
  *Salida: no tiene salida, solo afecta la interfaz
*/
void MainWindow::deleteElement()
{
    QList<Item*> elements = scene->selectedItems_();
    foreach(Item* i,elements)
        scene->deleteElement(i);
}

/*
  *Método slot exportFile: recibe signal de un QAction, lo que abre un dialogo de búsqueda en el
    sistema de carpetas y permite guardar un archivo de texto que contenga el estado actual
    de búsqueda(tablero).
  *Entradas: no recibe entradas
  *Salida: no tiene salida, solo afecta la interfaz
*/
void MainWindow::exportFile()
{
    QString fileName;
    fileName = QFileDialog::getSaveFileName(this,
         tr("Exportar Tablero"), "", tr("Archivo (*.txt)"));

    if(fileName == NULL)
        return;

    QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        return;

    QTextStream out(&file);

    char** board = scene->getBoard();
    int columns = scene->getColumnMax();
    int rows = scene->getRowMax();

    for(int i=0;i<rows;i++)
    {
        for(int j=0;j<columns;j++)
        {
            if(j == columns-1)
            {
                out << board[i][j];
            }else
            {
               out << board[i][j] << " ";
            }
        }
        out << "\n";
    }
    file.close();

    for(int i=0;i<rows;i++)
        delete board[i];
    delete board;
}

/*
  *Método slot help: recibe signal de un QAction, lo que abre un dialogo mostrando información
    relevanet a cerca del dfuncionamiento del programa(correcto uso)
  *Entradas: no recibe entradas
  *Salida: no tiene salida, solo afecta la interfaz
*/
void MainWindow::help()
{
    QTabWidget *helpWindow = new QTabWidget(this);

    helpWindow->setTabPosition(QTabWidget::West);

    QTextEdit *e = new QTextEdit();
    e->setHtml(tr("<b>Cargar configuración</b><br><br>"
                  "<ul>"
                  "<li type=\"circle\"> Para cargar un tablero desde un archivo se puede utilizar el botón de la "
                  "barra de herramientas o la opción <b><i>Importar Tablero</i></b> del menú <b><i>Archivo</i></b>.</li>"
                  "<li type=\"circle\"> Si se carga un tablero desde archivo este puede ser editado.</li>"
                  "</ul><br>"
                  "<center><img BORDER=3 src=\":/images/importarTablero.png\" alt=\"cargar archivo\" /> </center>"));
    QTextEdit *ee = new QTextEdit();
    ee->setHtml(tr("<b>Crear configuración</b>"
                  "<dl>"
                  "<dt><b><i>Nota:</i></b></dt>"
                  "<dd>Cuando se haga referencia a clic este significará <b><i>click izquierdo</i></b></dd>"
                  "</dl>"
                   "<dl>"
                   "<dt><b>Restricciones:</b><dt>"
                   "<dd>"
                   "<ul>"
                   "<li type=\"circle\"> No ejecutará hasta que exista como mínimo un carro en el tablero.</il>"
                   "<li type=\"circle\"> No ejecutará hasta que exista un carro de longitud <b><i>2</i></b>, horizontal e "
                   "identificado con la letra <b><i>'A'</i></b> en el tablero. </li>"
                   "<li type=\"circle\"> Al momento de agregar un elemento este debe de caber en el tablero,"
                   "de lo contrario no se podrá agregar. </li>"
                   "<li type=\"circle\"> Al momento de agregar un elemento este no se debe sobreponer a ningún otro elemento"
                   "existente, si esto ocurre el elemento no se podrá agregar.</li>"
                   "</ul>"
                   "</dd>"
                   "</dl>"
                   "<ul>"
                   "<li>Para la construción del tablero dinámico se usa el Panel de elementos.</li>"
                   "<li>Para agregar un nuevo elemento al tablero, se debe dar clic en alguno de los botones del Panel"
                   " de elementos, según se desee.</li>"
                   "<li>Si el elemento necesita un identificador se usará el que se encuentre seleccionado, este se puede cambiar "
                   "según la disponibilidad de identificadores, posteriormente se da clic en el tablero, el clic se debe de dar "
                   "en la posición donde se desea que empiece el elemento.</li>"
                   "<li>Una vez dado clic en algún botón del Panel de elementos, este no se desactivará, permitiendo agregar varios "
                   "elementos del mismo tipo si se continua dando click en el tablero, para desactivarlo se le debe volver a dar clic "
                   "al botón.</li>"
                   "<center><img BORDER=3 src=\":/images/crearTablero.png\" alt=\"crear tablero\" /> </center>"
                   "<li>Para eliminar elementos del tablero se debe de seleccionar el o los elementos a eliminar y luego dar clic en la <b>X</b> "
                   "de la barra de herramientas o en la opción <b><i>Eliminar</i></b> del menú <b><i>Elemento</i></b></li>"
                   "<li>Para seleccionar elementos asegurece de que ningún boton del panel de elementos se encuentra presionado, la seleción"
                   "se realiza dando doble clic al elemento. Si se desean seleccionar varios elementos, mantenga presionada la tecla "
                   "<b>Ctrl</b> mientras realiza la operación anteriormente descrita.</li>"
                   "<center><img BORDER=3 src=\":/images/eliminarElemento.png\" alt=\"una descripción univalle\" /> </center>"
                   "<li>En el tablero solo podrán existir elementos con identificador a excepción de los obstáculos, no se podrán agregar "
                   "elementos sin identificador. Lo identificadores empiezan en la letra <b>'A'</b> y terminan en la letra <b>'I'</b></li>"
                   "<li>  El tablero que muestre la aplicación puede ser guardado en un archivo de texto para su posterior carga. </li>"
                   "<center><img BORDER=3 src=\":/images/guardarTablero.png\" alt=\"una descripción univalle\" /> </center>"
                   "</ul>"));
    QTextEdit *eee = new QTextEdit();
    eee->setHtml(tr("<b>Ejecución</b>"
                    "<ul>"
                    "<li> La aplicacion tiene dos paneles, el <b>Panel de control</b> y el <b>Panel de elementos</b>.</li>"
                    "<li>Para ejecutar, se selecciona el tipo de búsqueda y el algoritmo paso siguiente "
                    "se hace clic en el botón <b>Ejecutar</b>.</li>"
                    "<li>El botón <b>Reiniciar tablero</b> sirve para restaurar las posiciones originales de los elementos del tablero "
                    "después de haber ejecutado un algoritmo, por eso solo estará disponible al terminar una ejecución.</li>"
                    "<li>El boton <b>Limpiar tablero</b> borra todos los elementos existentes en el tablero.</li>"
                    "<center><img BORDER=3 src=\":/images/ejecucion.png\" alt=\"una descripción univalle\" /> </center>"
                    "</ul>"));
    e->setReadOnly(true);
    ee->setReadOnly(true);
    eee->setReadOnly(true);
    helpWindow->addTab(e,"Cargar archivo");
    helpWindow->addTab(ee, "Crear tablero");
    helpWindow->addTab(eee, tr("Ejecución"));
    helpWindow->setGeometry(15,15,620,650);
    helpWindow->setWindowTitle("Ayuda proyecto 1 IA - Algoritmos de Busqueda - GPI");
    helpWindow->setWindowFlags(Qt::Dialog);
    helpWindow->show();
}

/*
  *Método slot importFile: recibe signal de un QAction, lo que abre un dialogo de búsqueda en el
    sistema de carpetas y permite cargar un archivo de texto que contenga el estado inicial
    de búsqueda(tablero) usando la clase Reader. Una vez cargado el archivo permite inicializar
    los estados de scene que posteriormente se usaran para ejecutar los algoritmos
  *Entradas: no recibe entradas
  *Salida: no tiene salida, solo afecta la interfaz
*/
void MainWindow::importFile()
{
    QString fileName;
    fileName = QFileDialog::getOpenFileName(this,
         tr("Importar Tablero"), "", tr("Archivo (*.txt)"));

    if(fileName == NULL)
        return;

    Reader reader;
    reader.load(fileName);
    reader.createStates();

    scene->setRowMax(reader.getHeight());
    scene->setColumnMax(reader.getWidth());
    scene->setBoard(reader.getCopyBoard(),reader.getHeight(),reader.getWidth());
    scene->setCars(reader.getStates());
    scene->setObstacles(reader.getObstacles());

    scene->drawGrid();

    runPushButton->setEnabled(true);
}

/*
  *Método slot itemDelete: recibe signal generada por el objeto scene, informando que un
    elemento gráfico a sido eliminado, con lo que la interfaz actualiza los
    identificadores dispodibles para nuevos elementos(habra uno más)
  *Entradas: un puntero que referencia a un objeto de tipo Item, esto es, el objeto recien
    eliminado
  *Salida: no tiene salida, solo afecta lel estado de la interfaz
*/
void MainWindow::itemDelete(Item *item)
{
    char l = item->getLetter();
    letterAvailable.append(QString(l));
    letterComboBox->clear();
    letterComboBox->addItems(letterAvailable);
}

/*
  *Método slot itemInserted: recibe signal generada por el objeto scene, informando que un
    nuevo elemento gráfico a sido insertado, con lo que la interfaz actualiza los
    identificadores dispodibles para nuevos elementos(habra uno menos)
  *Entradas: un puntero que referencia a un objeto de tipo Item, esto es, el objeto recien
    inertado
  *Salida: no tiene salida, solo afecta lel estado de la interfaz
*/
void MainWindow::itemInserted(Item* item)
{
    char l = item->getLetter();
    letterAvailable.removeOne(QString(l));
    letterComboBox->clear();
    letterComboBox->addItems(letterAvailable);
}

/*
  *Método slot letterCarChanged: recibe signal de un QCombobox representando si la letra que
    identifica a los carros en el tablero a cambiado, este cambio se lo comunica a scene
  *Entradas: recibe un String que es el valor enviado por el QCombobox
  *Salida: no tiene salida, solo afecta a scene
*/
void MainWindow::letterCarChange(const QString &value)
{
    scene->setLetterCarInserted(value);
}

/*
  *Método slot restartBoard: recibe signal de un QPushButton, esto hace que todos los
    graficos que han sido afectados por una animación vuelvan al lugar que les
    correspondian antes de ejecutada la animación. Usado cuando se ejecuta un algoritmo
    on un estado particular y desea ejecutar un nuevo algoritmo con el mismo estado
  *Entradas: no recibe entradas
  *Salida: no tiene salida, solo afecta la interfaz
*/
void MainWindow::restartBoard()
{
    scene->restartGraphics();
    restartPushButton->setEnabled(false);
    runPushButton->setEnabled(true);

    labelResultDepthTree->setText("");
    labelResultManyNodes->setText("");
    labelResultTime->setText("");
    labelResultCost->setText("");
}

/*
  *Método createWidgetResults: crea el panel(QWidget) donde se mostraran los resultados de las
    ejecuciones del algoritmo, es usado en la construcción del panel de control
  *Entradas: no recibe entradas
  *Salida: un puntero a a QWidget, este QWidget es el panel
*/
QWidget* MainWindow::createWidgetResults()
{
    QLabel *labelResult = new QLabel(tr("<b>Resultados de ejecuci&oacute;n</b>"));
    labelResult->setAlignment(Qt::AlignCenter);

    QLabel *labelResultDepthTreeName = new QLabel(tr("<b>Profundidad &aacute;rbol de b&uacute;squeda:</b>"));
    labelResultDepthTreeName->setAlignment(Qt::AlignLeft);

    labelResultDepthTree = new QLabel(tr(""));
    labelResultDepthTree->setAlignment(Qt::AlignLeft);

    QLabel *labelResultManyNodesName = new QLabel(tr("<b>Cantidad nodos expandidos:</b>"));
    labelResultManyNodesName->setAlignment(Qt::AlignLeft);

    labelResultManyNodes = new QLabel(tr(""));
    labelResultManyNodes->setAlignment(Qt::AlignLeft);

    QLabel *labelResultTimeName = new QLabel(tr("<b>Tiempo de ejecuci&oacute;n:</b>"));
    labelResultTimeName->setAlignment(Qt::AlignLeft);

    labelResultTime = new QLabel(tr(""));
    labelResultTime->setAlignment(Qt::AlignLeft);

    QLabel *labelResultCostName = new QLabel(tr("<b>Costo total de la soluci&oacute;n:</b>"));
    labelResultCostName->setAlignment(Qt::AlignLeft);

    labelResultCost = new QLabel(tr(""));
    labelResultCost->setAlignment(Qt::AlignLeft);

    QGridLayout *layout = new QGridLayout;

    layout->addWidget(labelResult,0,0,1,2);
    layout->setRowMinimumHeight(1,15);
    layout->addWidget(labelResultDepthTreeName,2,0);
    layout->addWidget(labelResultDepthTree,2,1);
    layout->addWidget(labelResultManyNodesName,3,0);
    layout->addWidget(labelResultManyNodes,3,1);
    layout->addWidget(labelResultTimeName,4,0);
    layout->addWidget(labelResultTime,4,1);
    layout->addWidget(labelResultCostName,5,0);
    layout->addWidget(labelResultCost,5,1);

    QWidget *widget = new QWidget;
    widget->setLayout(layout);

    return widget;
}

/*
  *Método slot runAlgorithm: recibe signal de un QPushButton, esto hace que se comience a
    ejecutar el tipo de búsqueda seleccionada, solo ejecuta si en el tablero existe al
    menos un carro, y si en el tablero existe un carro con id A, horizontal y de longitud 2.
    Utiliza otras clases para ejecutar la búsqueda, una vez obtenidad la solución llama a
    la clase correspondiente para que ejecute la animación y muestre el resultado.
  *Entradas: no recibe entradas
  *Salida: no tiene salida, solo afecta la interfaz
*/
void MainWindow::runAlgorithm()
{
    if(scene->carsMany() == 0)
        return;
    if(!scene->containsCar("A",Car::Horizontal,2))
        return;

    int algorithmType = algorithmTypeComboBox->currentIndex();
    int algorithm = algorithmComboBox->currentIndex();
    QVector<Action> solution;
    Search search(scene->getBoard(),scene->getCars());

    QCursor cursor(Qt::WaitCursor);
    setCursor(cursor);
    if(algorithmType == 0)//busqueda no informada
    {
        if(algorithm == 0)//preferente por amplitud
        {
            solution = search.amplitude();
            labelResultDepthTree->setText(QString::number(search.getDepthSearch()));
            labelResultManyNodes->setText(QString::number(search.getAmoutExpandSearch()));
            labelResultTime->setText(QString::number(search.getTotalTime()) + " seg");
            labelResultCost->setText(QString::number(search.getTotalCost()));

        }else if(algorithm == 1)//costo uniforme
        {
            solution = search.uniformCost();
            labelResultDepthTree->setText(QString::number(search.getDepthSearch()));
            labelResultManyNodes->setText(QString::number(search.getAmoutExpandSearch()));
            labelResultTime->setText(QString::number(search.getTotalTime()) + " seg");
            labelResultCost->setText(QString::number(search.getTotalCost()));

        }else if(algorithm == 2)//preferente por profundidad
        {
            solution = search.depth();
            labelResultDepthTree->setText(QString::number(search.getDepthSearch()));
            labelResultManyNodes->setText(QString::number(search.getAmoutExpandSearch()));
            labelResultTime->setText(QString::number(search.getTotalTime()) + " seg");
            labelResultCost->setText(QString::number(search.getTotalCost()));

        }

    }else if(algorithmType == 1)//busqueda informada
    {
        if(algorithm == 0)//avara
        {
            solution = search.greedy();
            labelResultDepthTree->setText(QString::number(search.getDepthSearch()));
            labelResultManyNodes->setText(QString::number(search.getAmoutExpandSearch()));
            labelResultTime->setText(QString::number(search.getTotalTime()) + " seg");
            labelResultCost->setText(QString::number(search.getTotalCost()));

        }else if(algorithm == 1)//a*
        {
            solution = search.aStar();
            labelResultDepthTree->setText(QString::number(search.getDepthSearch()));
            labelResultManyNodes->setText(QString::number(search.getAmoutExpandSearch()));
            labelResultTime->setText(QString::number(search.getTotalTime()) + " seg");
            labelResultCost->setText(QString::number(search.getTotalCost()));
        }
    }
    cursor.setShape(Qt::ArrowCursor);
    setCursor(cursor);

    scene->runAnimation(solution);

    restartPushButton->setEnabled(true);
    runPushButton->setEnabled(false);
}
