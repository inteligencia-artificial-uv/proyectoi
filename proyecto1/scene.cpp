/*
  *Nombre archivo: scene.cpp
  *Autor: Cristian Leonardo Ríos López
  *Fecha de creación: Septiembre 2011
  *Fecha de ultima modificación: Diciembre 2011
  *Universidad del Valle
*/

#include "scene.h"
#include <QDebug>
#include <QGraphicsLineItem>
#include <QList>
#include <QGraphicsSceneMouseEvent>
#include <QSequentialAnimationGroup>
#include <QPropertyAnimation>

Scene::Scene(int rowsMax, int columnsMax, QObject *parent)
 : QGraphicsScene(parent)
{
    this->columnMax= columnsMax;
    this->rowMax= rowsMax;
    board= new char*[this->rowMax];
    for(int i=0; i<this->rowMax; i++)
    {
       board[i]= new char[this->columnMax];
    }
    for(int i=0;i<this->rowMax;i++)
    {
        for(int j=0;j<this->columnMax;j++)
        {
            board[i][j] = '0';
        }
    }
    myType = Scene::Libre;
    this->setBackgroundBrush(Qt::black);
}

Scene::~Scene()
{
    for(int i=0; i< this->rowMax; i++)
    {
        delete this->board[i];
        this->board[i]=0;
    }

    delete this->board;
    this->board=0;
}

/*
  *Metodo carsMany: retona el valor de la cantidad de carros que se encuentran en el tablero.
  *Entrada: No tiene parametros de entrada.
  *Salida: Entero del tamaño del vector que contiene los carros del tablero.
 */
int Scene::carsMany()
{
    return cars.size();
}

/*
  *Metodo clearBoard: limpia todos los elementos del tablero. el tablero queda vacio.
  *Entrada: No tiene parametros de entrada.
  *Salida: La modificacion de los vectores que contiene los graficos de los carros y los obstaculos
  *        dejandolos vacios.
*/
void Scene::clearBoard()
{
    foreach(Item* i, carsGraphics)
        deleteElement(i);
    foreach(Item* i, obstaclesGraphics)
        deleteElement(i);

    cars.clear();
    obstacles.clear();
}

/*
  *Metodo containsCar: Valida si un carro se encuentra entre los carros que hay en el tablero.
  *Entradas: letter que corresponde al identificador del carro, direction revela la dirección del carro y size
  *          que corresponde al tamaño del carro que se busca.
  *Salida: un booleano que tomará el valor false cuando dicho carro no se enuentre, de lo contrario true.
*/
bool Scene::containsCar(QString letter, Car::directionValue direction, int size)
{
    bool contains = false;
    char l = letter.unicode()->toAscii();
    if((direction == Car::Ninguna))
    {
        if(size == -1)
        {
            foreach(Car car, cars)
            {
                if(car.getId() == l)
                {
                    contains = true;
                    break;
                }
            }
        }else
        {
            foreach(Car car, cars)
            {
                if((car.getId() == l) && (car.getSize() == size))
                {
                    contains = true;
                    break;
                }
            }
        }
    }else
    {
        if(size == -1)
        {
            foreach(Car car, cars)
            {
                if((car.getId() == l) && (car.getDirection() == direction))
                {
                    contains = true;
                    break;
                }
            }
        }else
        {
            foreach(Car car, cars)
            {
                if((car.getId() == l) && (car.getSize() == size) && (car.getDirection() == direction))
                {
                    contains = true;
                    break;
                }
            }
        }
    }

    return contains;
}

/*
  *Metodo deleteElement: permite borrar el grafico de un carro u obstaculo del tablero y del vector de carros que
  *                      contiene los carros que estan actualmente en el tablero o del vectore que contiene los
  *                      obstaculos que estan en el tablero.
  *Entrada: item que representa el carro u obstaculo a borrar.
  *Salida: la modificacion del vector de graficos(carros u obstaculos) del tablero, del vector de carros del
  *        tablero o vector de obstaculos y del enumerator eliminando la referencia a dicho item(carro/obstaculo).
*/
void Scene::deleteElement(Item* item)
{
    //Item* itemCast= qgraphicsitem_cast<Item *>(item);

    for(int i=0;i<carsGraphics.size();i++)
    {
        Car c = cars.at(i);
        if(item->getLetter() == c.getId())
        {
            cars.remove(i);
            carsGraphics.remove(i);
            removeItem(item);
            updateBoard(c.getPosition(),typeCar(c),0,'Z');//es irrelevante el ultimo argumento
            emit itemDelete(item);
            break;
        }
    }

    for(int i=0;i<obstaclesGraphics.size();i++)
    {
        int x = item->pos().y()/item->getHeight();
        int y = item->pos().x()/item->getWidth();

        QPoint r(x,y);
        int index = obstacles.indexOf(r);
        if(index>-1)
        {
            obstacles.remove(index);
            obstaclesGraphics.remove(index);
            removeItem(item);
            updateBoard(r,Obstaculo,0,'Z');//es irrelevante el ultimo argumento
            break;
        }
    }

    //se realiza la busqueda secuencial puesto que la cantidad de elementos existentes no es tan grande.
}

/*
  *Metodo drawGrid: Dibuja el tablero, lineas horizontales y verticales, y hace el llamado a drawElements.
  *Entrada: No tiene parametros de entrada.
  *Salida: La modificacion del QGraphicsScene mostrando las lineas del tablero y los elementos.
*/
void Scene::drawGrid()
{
    double dx = width()/this->columnMax;
    double dy = height()/this->rowMax;

    /*limpiar el tablero*/
    //this->clear(); //No se puede usar clear, este destruye la referencia a los objetos en el vector de graphics
    QList<QGraphicsItem*> elements = items();
    foreach(QGraphicsItem* i,elements)
    {
        removeItem(i);
    }

    /*dibujar lienas verticales*/
    for(int i=0;i<=this->columnMax;i++)
    {
        QGraphicsLineItem *line = new QGraphicsLineItem(i*dx,0.0,i*dx,height());
        QPen pen(QColor(Qt::white));
        line->setPen(pen);
        addItem(line);
    }

    /*dibujar lineas horizontales*/
    for(int j=0;j<=this->rowMax;j++)
    {
        QGraphicsLineItem *line = new QGraphicsLineItem(0.0,j*dy,width(),j*dy);
        QPen pen(QColor(Qt::white));
        line->setPen(pen);
        addItem(line);
    }

    drawElements();
}

/*
  *Metodo drawElements: Dibuja los carros y obstaculos en el tablero.
  *Entrada: No tiene parametros de entrada.
  *Salida: Modifica el QGraphicsScene dibujando los carros y obstaculos.
*/
void Scene::drawElements()
{
    /*tener cuidado de llamar a este metodo, se deben haber borrado los elementos del graphics viewer*/
    for(int i=0;i<carsGraphics.size();i++)
    {
        Item* item = carsGraphics.at(i);
        addItem(item);
        emit itemInserted(item);
    }
    for(int i=0;i<obstaclesGraphics.size();i++)
        addItem(obstaclesGraphics.at(i));
}


/*
  *Metodo findCar: Busca un carro, en los carros dibujados (vector carGraphics).
  *Entrada: id que representa el id del carro a buscar.
  *Salida: el item que representa el carro.
*/
Item* Scene::findCar(char id)
{
    Item* car = 0;
    for(int i=0;i<carsGraphics.size();i++)
    {
        if(carsGraphics.at(i)->getLetter() == id)
        {
            car = carsGraphics.at(i);
            break;
        }
    }

    return car;
}

/*
  *Metodo getRowMax: retorna el numero de filas del tablero.
  *Entrada: No tiene parametros de entrada.
  *Salida: El entero que corresponde a las filas del tablero
*/
int Scene::getRowMax()
{
    return rowMax;
}

/*
  *Metodo getColumnMax: retorna el valor de las columnas del tablero.
  *Entrada: No tiene parametros de entrada.
  *Salida: El enterio que corresponde al valor de las columnas del tablero.
*/
int Scene::getColumnMax()
{
    return columnMax;
}

/*
  *Metodo getBoard: retorna una copia del tablero.
  *Entrada: No tiene parametros de entrada.
  *Salida: Un arreglo que representa el tablero.
*/
char** Scene::getBoard()
{
    char** boardCopy= new char*[rowMax];
    for(int i=0; i<rowMax; i++)
    {
       boardCopy[i]= new char[columnMax];
    }

    for(int i=0;i<rowMax;i++)
    {
        for(int j=0;j<columnMax;j++)
        {
            boardCopy[i][j] = board[i][j];
        }
    }

    return boardCopy;
}

/*
  *Metodo getCars: retorna el vector de carros que estan en el tablero.
  *Entrada: No tiene parametros de entrada.
  *Salida: El vector de carros que estan en el tablero.
*/
QVector<Car> Scene::getCars()
{
    return cars;
}

/*
  *Metodo getLetterCarInserted: retorna el id del carro agregado al tablero.
  *Entrada: No tiene parametros de entrada.
  *Salida: retorna el identificador del carro que se ha ingresado en el tablero.
*/
QString Scene::getLetterCarInserted()
{
    return letterCarInserted;
}

/*
  *Metodo howPositionBoard: retorna la posicion en el tablero(archivo entrada) dada una posicion en el grafico del tablero.
  *Entrada: point representa la posicion de un elemento en el tablero grafico.
  *Salida: Qpoint que representa la posicion en el tablero(archivo entrada).
*/
QPoint Scene::howPositionBoard(QPointF point)
{
    double dx = width()/columnMax;
    double dy = height()/rowMax;

    QPoint position(-1,-1);

    for(int i=0;i<columnMax;i++)
    {
        if((point.x() >= dx*i) && (point.x() < dx*(i+1)))
        {
            for(int j=0;j<rowMax;j++)
            {
                if((point.y() >= dy*j) && (point.y() < dy*(j+1)))
                {
                    position.setX(j);
                    position.setY(i);
                }
            }
        }
    }

    return position;
}

/*
  *Metodo items_: encuentra todos los elementos gráficos que se encuentren
    en la escena, usa el método Items de scene pero como este método retorna
    QGraphicsItem se debe hacer una cast a Item, puesto
    que los gráficos que hay en la escena son de tipo Item,
    y si no se hace el cast se perderia información necesaria de los objetos.
    item es una clase que hereda de QGraphicsItem
  *Entrada: no tiene entradas
  *Salida: una lista de punteros a objetos tipo Item
*/
QList<Item*> Scene::items_()
{
    QList<QGraphicsItem*> it = items();
    QList<Item*> listItems;
    foreach(QGraphicsItem* i,it)
    {
        listItems.append(qgraphicsitem_cast<Item *>(i));
    }
    return listItems;
}

/*
  *Metodo mousePressEvent: recibe un evento del mouse si este es un clic derecho, se hace en una posicion valida del tablero
  *                        y no interfiere con otro elemento entonces se inserta el elemento que se halla elegido en la interfaz.
  *Entrada: mouseEvent representa el clic del mouse hecho en el tablero.
  *Salida: el ingreso del elemento(grafico) en el tablero, por supuesto si es valida la operacion.
*/
void Scene::mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
    if(mouseEvent->button() != Qt::LeftButton)
        return;
    if((letterCarInserted == "") && (myType != Obstaculo))
        return;
    if(myType == Libre)
        return;

    QPoint position = howPositionBoard(mouseEvent->scenePos());

    if(!validatePosition(position))/*el elemento se insertara en una posicion validad del tablero*/
        return;

    if(overlapElements(position))/*el elemento que se desea insertar se va a sobrelapar a otro existente*/
        return;

    updateBoard(position,myType,1,letterCarInserted.unicode()->toAscii());//se agrega el nuevo item a la matriz

    double dx = width()/columnMax;
    double dy = height()/rowMax;

    if(myType == Obstaculo)
    {
        QPixmap pix(":/images/galaxia5.png");
        Item* item = new Item(pix,dx,dy,dx*position.y(),dy*position.x(),'Z');//el id es irrelevante para los obstaculos
        obstaclesGraphics.append(item);
        obstacles.append(position);
        drawGrid();
    }else if(myType == AutoDosH)
    {
        char id = letterCarInserted.unicode()->toAscii();
        Car car(id,position,2,Car::Horizontal);
        cars.append(car);
        QString image  = whichImage(Car::Horizontal,id,car.getSize());
        QPixmap pix(image);
        Item* item = new Item(pix,dx*car.getSize(),dy,dx*car.getPosition().y(),dy*car.getPosition().x(),id);
        carsGraphics.append(item);
        drawGrid();

    }else if(myType == AutoDosV)
    {
        char id = letterCarInserted.unicode()->toAscii();
        Car car(id,position,2,Car::Vertical);
        cars.append(car);
        QString image  = whichImage(Car::Vertical,id,car.getSize());
        QPixmap pix(image);
        Item* item = new Item(pix,dx,dy*car.getSize(),dx*car.getPosition().y(),dy*car.getPosition().x(),id);
        carsGraphics.append(item);
        drawGrid();

    }else if(myType == AutoTresH)
    {
        char id = letterCarInserted.unicode()->toAscii();
        Car car(id,position,3,Car::Horizontal);
        cars.append(car);
        QString image  = whichImage(Car::Horizontal,id,car.getSize());
        QPixmap pix(image);
        Item* item = new Item(pix,dx*car.getSize(),dy,dx*car.getPosition().y(),dy*car.getPosition().x(),id);
        carsGraphics.append(item);
        drawGrid();

    }else if(myType == AutoTresV)
    {
        char id = letterCarInserted.unicode()->toAscii();
        Car car(id,position,3,Car::Vertical);
        cars.append(car);
        QString image  = whichImage(Car::Vertical,id,car.getSize());
        QPixmap pix(image);
        Item* item = new Item(pix,dx,dy*car.getSize(),dx*car.getPosition().y(),dy*car.getPosition().x(),id);
        carsGraphics.append(item);
        drawGrid();
    }

    QGraphicsScene::mousePressEvent(mouseEvent);
}


/*
  *Metodo overlapElements: verifica si un elemento que se desea ingresar en determinada posicion, solaparía un elemento
  *                        ya existente en dicha posicion.
  *Entrada: position que representa la posicion en la cual se quiere ingresar un elemento.
  *Salida: un booleano que indica sí solapa o no el nuevo elemento a ingresar.
*/
bool Scene::overlapElements(QPoint position)
{
    bool overlap = false;
    int x = position.x();
    int y = position.y();

    if(board[x][y] != '0')
    {
        overlap = true;
    }else if(myType == AutoDosH)
    {
        if(board[x][y+1] != '0')
            overlap = true;
    }else if(myType == AutoDosV)
    {
        if(board[x+1][y] != '0')
            overlap = true;
    }else if(myType == AutoTresH)
    {
        if((board[x][y+1] != '0') || (board[x][y+2] != '0'))
            overlap = true;
    }else if(myType == AutoTresV)
    {
        if((board[x+1][y] != '0') || (board[x+2][y] != '0'))
            overlap = true;
    }

    return overlap;
}

/*
  *Metodo restartGraphics: reinicia la posicion de todos los elementos que estan en el tablero.
  *Entrada: No tiene parametros de entrada.
  *Salida: la modificacion de cada elemento y mostrarlos en la posicion inicial en el tablero.
*/
void Scene::restartGraphics()
{
    foreach(Item* i,carsGraphics)
    {
        i->setPos(i->getXstart(),i->getYstart());
        i->setX(i->getXstart());
        i->setY(i->getYstart());
    }
}

/*
  *Metodo runAnimation: muestra como los carros se mueven apartir de un vector de acciones (movimientos).
  *Entrada: movements vector de action que represeta los pasos para irse moviendo y llegar a la meta.
  *Salida: la animacion en el QGraphicsScene de los elementos(carros).
*/
void Scene::runAnimation(QVector<Action> movements)
{
    double dx = width()/columnMax;
    double dy = height()/rowMax;

    QSequentialAnimationGroup* group = new QSequentialAnimationGroup;
    for(int i=0;i<movements.size();i++)
    {
        Action action = movements.at(i);
        char id = action.getIdCar();
        Action::move direction = action.getDirection();
        int moving = action.getMoving();
        Item* car = findCar(id);

        QPropertyAnimation* anim = new QPropertyAnimation(car,"pos");
        anim->setDuration(1000*moving);
        anim->setStartValue(QPoint(car->getX(),car->getY()));
        anim->setEasingCurve(QEasingCurve::InOutBack);

        int xEnd;
        int yEnd;

        switch(direction)
        {
            case Action::Derecha:
                xEnd = car->getX()+(moving*dx);
                yEnd = car->getY();
                anim->setEndValue(QPoint(xEnd,yEnd));
                car->setX(xEnd);
                car->setY(yEnd);
                break;
            case Action::Arriba:
                xEnd = car->getX();
                yEnd = car->getY()-(moving*dy);
                anim->setEndValue(QPoint(xEnd,yEnd));
                car->setX(xEnd);
                car->setY(yEnd);
                break;
            case Action::Izquierda:
                xEnd = car->getX()-(moving*dx);
                yEnd = car->getY();
                anim->setEndValue(QPoint(xEnd,yEnd));
                car->setX(xEnd);
                car->setY(yEnd);
                break;
            case Action::Abajo:
                xEnd = car->getX();
                yEnd = car->getY()+(moving*dy);
                anim->setEndValue(QPoint(xEnd,yEnd));
                car->setX(xEnd);
                car->setY(yEnd);
                break;
        }

        group->addAnimation(anim);
    }

    group->start();
}

/*
  *Metodo selectedItems_: encuentra todos los elementos gráficos que se encuentren
    seleccionados en la escena, usa el método selectedItems de scene
    pero como este método retorna QGraphicsItem se debe hacer una cast a Item, puesto
    que los gráficos que hay en la escena que se pueden seleccionar son de tipo Item,
    y si no se hace el cast se perderia información necesaria de los objetos.
    item es una clase que hereda de QGraphicsItem
  *Entrada: no tiene entradas
  *Salida: una lista de punteros a objetos tipo Item
*/
QList<Item*> Scene::selectedItems_()
{
    QList<QGraphicsItem*> items = selectedItems();
    QList<Item*> listItems;
    foreach(QGraphicsItem* i,items)
    {
        listItems.append(qgraphicsitem_cast<Item *>(i));
    }
    return listItems;
}

/*
  *Metod setBoard: cambia el tablero con la informacion del tablero de entrada.
  *Entradas: board representa un tablero, rows representa las filas del tablero y columns las columnas del tablero.
  *Salida: la modificacion del tablero con la nueva información.
*/
void Scene::setBoard(char** board, int rows, int columns)
{
    for(int i=0;i<rows;i++)
    {
        for(int j=0;j<columns;j++)
        {
            this->board[i][j] = board[i][j];
        }
    }
}

/*
  *Metodo setCars: Cambia el valor del vector cars por el nuevo vector, y añade los elementos al vector carsGraphics.
  *Entrada: cars que es un vector de Car que representa los carros a añadirse al tablero.
  *Salida: la modificacion del vector cars y carsGrapchics con los carros del vector de entrada cars.
*/
void Scene::setCars(QVector<Car> cars)
{
    this->cars = cars;

    double dx = width()/columnMax;
    double dy = height()/rowMax;

    carsGraphics.clear();
    for(int i=0;i<cars.size();i++)
    {
        Car car = cars.at(i);
        char id = car.getId();
        Car::directionValue direction = car.getDirection();
        QString image = whichImage(direction,id,car.getSize());

        if(direction == Car::Horizontal)
        {
            QPixmap pix(image);
            Item* item = new Item(pix,dx*car.getSize(),dy,dx*car.getPosition().y(),dy*car.getPosition().x(),id);
            carsGraphics.append(item);
        }else // vertical
        {
            QPixmap pix(image);
            Item* item = new Item(pix,dx,dy*car.getSize(),dx*car.getPosition().y(),dy*car.getPosition().x(),id);
            carsGraphics.append(item);
        }
    }
}

/*
  *Metodo setColumnMax: Cambia el valor de columnMax por un nuevo valor.
  *Entrada: columnMax entero que representa el numero de columnas del tablero.
  *Salida: La modificacion del atributo columnMax con el nuevo valor.
*/
void Scene::setColumnMax(int columnMax)
{
    this->columnMax = columnMax;
}

/*
  *Metodo setLetterCarInserted: modifica el id del carro insertado por uno que le dan de entrada.
  *Entrada: letter que representa el identificador del carro insertado.
  *Salida: el cambio del id del carro insertado, modifica el atributo letterCarInserted.
*/
void Scene::setLetterCarInserted(const QString letter)
{
    letterCarInserted = letter;
}

/*
  *Metodo setObstacles: modifica los obstaculos con la nueva información, a su vez modifica la informacion en el vector
  *                     obstaclesGraphics.
  *Entrada: obstacles que es un vetor de QPoint que representa la información de los obstaculos.
  *Salida: la modificacion del atributo obstacles y de obstaclesGraphics.
*/
void Scene::setObstacles(QVector<QPoint> obstacles)
{
    this->obstacles = obstacles;

    double dx = width()/columnMax;
    double dy = height()/rowMax;

    obstaclesGraphics.clear();
    for(int i=0;i<obstacles.size();i++)
    {
        QPixmap pix(":/images/galaxia5.png");
        Item* item = new Item(pix,dx,dy,dx*obstacles.at(i).y(),dy*obstacles.at(i).x(),'Z');//el id es irrelevante para los obstaculos
        obstaclesGraphics.append(item);
    }
}

/*
  *Metodo setRowMax: cambia el valor del atributo rowMax por un nuevo valor.
  *Entrada: rowMax entero que representa el nuevo valor de filas del tablero.
  *Salida: la modificacion del atributo rowMax.
*/
void Scene::setRowMax(int rowMax)
{
    this->rowMax = rowMax;
}

/*
  *Metodo setType: modifica el valor del type por un nuevo valor.
  *Entrada: type que representa el nuevo tipo de clic.
  *Salida: la modificacion del atributo myType pro el nuevo valor.
*/
void Scene::setType(Type type)
{
    myType = type;
}

/*
  *Metodo typeCar: retorna el tipo de carro que debe ser un car según condiciones de dirección y tamanio.
  *Entrada: la referencia de un car del cual queremos obtener el tipo.
  *Salida: el tipo del carro.
*/
Scene::Type Scene::typeCar(Car &car)
{
    Type type;
    int size = car.getSize();
    Car::directionValue direction = car.getDirection();

    if(size == 2)
    {
        if(direction == Car::Horizontal)
        {
            type = AutoDosH;
        }else//direction == Car::Vertical
        {
            type = AutoDosV;
        }
    }else //sise == 3
    {
        if(direction == Car::Vertical)
        {
            type = AutoTresV;
        }else //direction == Car::Horizontal
        {
            type = AutoTresH;
        }
    }

    return type;
}

/*
  *Metodo updateBoard: Actualiza los valores que tiene board en cada una de sus celdas
  *Entradas: posistion que representa la posicion a actualizar, type que representa el tipo del elemento, action representa si
  *          se va a eliminar '0' o agregar elemento '1' y letter representa el id del carro por si el elemento es un carro.
  *Salida: la modificcion del atributo board con la nueva información.
*/
void Scene::updateBoard(QPoint position, Type type, int action, char letter)
{
    int x = position.x();
    int y = position.y();

    //action vale 1 para agregar y 0 para eliminar elementos del tablero
    if(action == 0)
    {
        if(type == AutoDosH)
        {
            board[x][y] = '0';
            board[x][y+1] = '0';
        }else if(type == AutoDosV)
        {
            board[x][y] = '0';
            board[x+1][y] = '0';
        }else if(type == AutoTresH)
        {
            board[x][y] = '0';
            board[x][y+1] = '0';
            board[x][y+2] = '0';
        }else if(type == AutoTresV)
        {
            board[x][y] = '0';
            board[x+1][y] = '0';
            board[x+2][y] = '0';
        }else if(type == Obstaculo)
        {
            board[x][y] = '0';
        }
    }else if(action == 1)
    {
        if(type == AutoDosH)
        {
            board[x][y] = letter;
            board[x][y+1] = letter;
        }else if(type == AutoDosV)
        {
            board[x][y] = letter;
            board[x+1][y] = letter;
        }else if(type == AutoTresH)
        {
            board[x][y] = letter;
            board[x][y+1] = letter;
            board[x][y+2] = letter;
        }else if(type == AutoTresV)
        {
            board[x][y] = letter;
            board[x+1][y] = letter;
            board[x+2][y] = letter;
        }else if(type == Obstaculo)
        {
            board[x][y] = '1';
        }
    }
}


/*
  *Metodo validatePosition: verifica si una posicion en el tablero (dada con el mouse) es valida para ingresar elementos.
  *Entrada: position que representa el punto donde se dio clic.
  *Salida: el booleano que responde con true si la posicion es valida en caso contrario false.
*/
bool Scene::validatePosition(QPoint position)
{
    bool validate = false;

    int x = position.x();
    int y = position.y();

    if((x != -1) && (y != -1))
    {
        if(myType == AutoDosH)
        {
            if(y+2 <= rowMax)
                validate = true;
        }else if(myType == AutoDosV)
        {
            if(x+2 <= columnMax)
                validate = true;
        }else if(myType == AutoTresH)
        {
            if(y+3 <= rowMax)
                validate = true;
        }else if(myType ==  AutoTresV)
        {
            if(x+3 <= columnMax)
                validate = true;
        }else if(myType == Obstaculo)
        {
            validate = true;
        }
    }

    return validate;
}

/*
  *Metodo whichImage: verifica que imagen se debe usar para determinado carro.
  *Entrada: direction representa la direccion del carro, letter el id del carro y size el tamaño del carro.
  *Salida: la string con el nombre de la imagen a usar.
*/
QString Scene::whichImage(Car::directionValue direction,char letter, int size)
{
    QString image;
    if(direction == Car::Horizontal)
    {
        if(size == 2)
        {
            switch(letter)
            {
                case 'A':
                    image = ":/images/h2a_.png";
                    break;
                case 'B':
                    image = ":/images/h2b_.png";
                    break;
                case 'C':
                    image = ":/images/h2c_.png";
                    break;
                case 'D':
                    image = ":/images/h2d_.png";
                    break;
                case 'E':
                    image = ":/images/h2e_.png";
                    break;
                case 'F':
                    image = ":/images/h2f_.png";
                    break;
                case 'G':
                    image = ":/images/h2g_.png";
                    break;
                 case 'H':
                    image = ":/images/h2h_.png";
                    break;
                case 'I':
                    image = ":/images/h2i_.png";
                    break;
            }
        }else if(size == 3)
        {
            switch(letter)
            {
                case 'A':
                    image = ":/images/h3a_.png";
                    break;
                case 'B':
                    image = ":/images/h3b_.png";
                    break;
                case 'C':
                    image = ":/images/h3c_.png";
                    break;
                case 'D':
                    image = ":/images/h3d_.png";
                    break;
                case 'E':
                    image = ":/images/h3e_.png";
                    break;
                case 'F':
                    image = ":/images/h3f_.png";
                    break;
                case 'G':
                    image = ":/images/h3g_.png";
                    break;
                 case 'H':
                    image = ":/images/h3h_.png";
                    break;
                case 'I':
                    image = ":/images/h3i_.png";
                    break;
            }
        }
    }else if(direction == Car::Vertical)
    {
        if(size == 2)
        {
            switch(letter)
            {
                case 'A':
                    image = ":/images/v2a_.png";
                    break;
                case 'B':
                    image = ":/images/v2b_.png";
                    break;
                case 'C':
                    image = ":/images/v2c_.png";
                    break;
                case 'D':
                    image = ":/images/v2d_.png";
                    break;
                case 'E':
                    image = ":/images/v2e_.png";
                    break;
                case 'F':
                    image = ":/images/v2f_.png";
                    break;
                case 'G':
                    image = ":/images/v2g_.png";
                    break;
                case 'H':
                    image = ":/images/v2h_.png";
                    break;
                case 'I':
                    image = ":/images/v2i_.png";
                    break;
            }
        }else if(size == 3)
        {
            switch(letter)
            {
                case 'A':
                    image = ":/images/v3a_.png";
                    break;
                case 'B':
                    image = ":/images/v3b_.png";
                    break;
                case 'C':
                    image = ":/images/v3c_.png";
                    break;
                case 'D':
                    image = ":/images/v3d_.png";
                    break;
                case 'E':
                    image = ":/images/v3e_.png";
                    break;
                case 'F':
                    image = ":/images/v3f_.png";
                    break;
                case 'G':
                    image = ":/images/v3g_.png";
                    break;
                case 'H':
                    image = ":/images/v3h_.png";
                    break;
                case 'I':
                    image = ":/images/v3i_.png";
                    break;
            }
        }
    }
    return image;
}
