/*

* Nombre archivo : action.cpp
* Nombre de la clase : Action
* Autor: Maria Andrea Cruz Blandon
         Cristian Leonardo Rios
         Yerminson Doney Gonzalez Muñoz
* Fecha de creación: Septiembre del 2011
* Fecha de modificación: Diciembre del 2011
*
* Universidad del Valle
*/
#include "action.h"

Action::Action()
{
    this->idCar = ' ';
    this->direction = Izquierda;
    this->moving = -1;
}

Action::Action(char idCar,move direction,int moving)
{
    this->idCar = idCar;
    this->direction = direction;
    this->moving = moving;
}

/*
* Metodo setIdCar: Permite cambiar el identificador de un carro, asignandole una nueva letra.*
* Entradas: Un caracter que representa la letra por la cual se va a modificar el carro en la accion.
* Salida: No hay salida, ya que solo representa un cambio en el atributo idCar en la accion.
*/
void Action::setIdCar(char idCar)
{
    this->idCar = idCar;
}

/*
* Metodo setDirection: Permite cambiar la direccion en la cual se esta realizando la accion de un carro, asignandole una nueva direccion.
* Entradas: Una direccion que representa la nueva direccion en la cual se va a realizar la accion.
* Salida: No hay salida, ya que solo representa un cambio en el atributo direction en la accion.
*/
void Action::setDirection(move direction)
{
    this->direction = direction;
}

/*
* Metodo setMoving: Permite cambiar las unidades que un carro se desplaza mediante una accion, asignandole una nueva cantidad.
* Entradas: Un numero que representa la cantidad de unidades que va a desplazar un carro con esta acccion.
* Salida: No hay salida, ya que solo representa un cambio en el atributo moving en la accion.
*/
void Action::setMoving(int moving)
{
    this->moving = moving;
}

/*
* Metodo getIdCar: Permite obtener el identificador de un carro sobre el cual se esta realizando una accion.
* Entradas: No hay entrada, ya que solo se esta realizando una consulta.
* Salida: Un caracter que representa la letra del carro sobre el que se realiza la accion.
*/
char Action::getIdCar()
{
    return idCar;
}

/*
* Metodo getDirection : Permite obtener la direccion en la cual se esta realizando la accion.
* Entradas: No hay entrada, ya que solo se esta realizando una consulta.
* Salida: Un movimiento(move) que indica la direccion en la cual se esta realizando la accion.
*/
Action::move Action::getDirection()
{
    return direction;
}

/*
* Metodo getMoving: Permite obtener la cantidad que se desplaza un carro con la accion.
* Entradas: No hay entrada , ya que solo es una consulta.
* Salida: Un numero entero que representa la cantidad que se desplaaz el carro con la accion.
*/
int Action::getMoving()
{
    return moving;
}

/*
* Metodo toString: Permite obtener informacion detallada sobre la accion en forma de una cadena de caracteres.
* Entradas: No  hay entrada, ya que solo es una consulta.
* Salida: Un Qstring(cadena) que representa de manera organizada la informacion de una  accion que puede realizar un carro
*/
 QString Action::toString()
 {
     QString action="idCar: "+ QString(this->idCar)+" - Dir: "+ QString::number(this->direction) + " - Moving: "+QString::number(this->moving);
     return action;

 }
