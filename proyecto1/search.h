/*
  *Nombre archivo: scene.h
  *Autor: María Andrea Cruz Blandón
  *       Yerminson Doney Gonzalez Muñoz
  *       Cristian Leonardo Ríos López
  *Fecha de creación: Septiembre 2011
  *Fecha de ultima modificación: Diciembre 2011
  *Descripción: La clase Search perite realizar las busquedas informada, y no informadas. usando estructuras como
  *             pila QStack para el algoritmo busqueda por profundidad, lista enlazada QLinkedList para el algoritmo
  *             busqueda por amplitud y cola de prioridad priority_queue para el algoritmo de costo uniforme,
  *             avara y a estrella.
  *Universidad del Valle
*/

#ifndef SEARCH_H
#define SEARCH_H

#include <QVector>
#include "action.h"
#include "car.h"
#include "nodo.h"

class Search
{
private:
    Nodo* rootNode;
    QVector<Action> actions;//el que sera retornado

    int widhtBoard;
    int heigthBoard;

    int depthSearch;
    int amoutExpandSearch;
    int totalCost;
    double totalTime;

    Nodo* applyAction(Nodo* nodo,Action &action);
    QVector<Action> buildPath(Nodo* goalNodo);
    void deleteCar(char** board, Car &car);
    QVector<Nodo*> expand(Nodo* nodo);
    QVector<Action> findActions(char** board, QVector<Car> &states);
    int findFreeSpace(int x, int y, Action::move moving, char** copy);
    int heuristicSearch(QVector<Car> states, char** board);
    void insertCar(char** board, Car &car);
    bool isGoal(QVector<Car> states, char** board);
    int searchIndexCar(char letter, QVector<Car> &states);
    bool testPosition(int x, int y);

public:
    Search(char **board,QVector<Car> states);
    ~Search();

    QVector<Action> amplitude();
    QVector<Action> depth();
    QVector<Action> uniformCost();
    QVector<Action> greedy();
    QVector<Action> aStar();

    int getDepthSearch();
    int getAmoutExpandSearch();
    int getTotalCost();
    double getTotalTime();

};
//By Timmy Tales
#endif // SEARCH_H
